<?php
/**
 * This class allows to define application profile attribute entity collection class.
 * Application profile attribute entity collection is a save attribute entity collection,
 * used to store application profile attribute entities.
 * key: attribute key => application profile attribute entity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\attribute\model;

use liberty_code\attribute_model\attribute\repository\model\SaveAttributeEntityCollection;

use people_sdk\app_profile\attribute\model\AppProfileAttrEntity;



/**
 * @method null|AppProfileAttrEntity getItem(string $strKey) @inheritdoc
 * @method null|AppProfileAttrEntity getObjAttribute(string $strName) @inheritdoc
 * @method string setItem(AppProfileAttrEntity $objEntity) @inheritdoc
 * @method string setAttribute(AppProfileAttrEntity $objAttribute) @inheritdoc
 * @method AppProfileAttrEntity removeItem($strKey) @inheritdoc
 * @method AppProfileAttrEntity removeAttribute(string $strName) @inheritdoc
 */
class AppProfileAttrEntityCollection extends SaveAttributeEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return AppProfileAttrEntity::class;
    }



}
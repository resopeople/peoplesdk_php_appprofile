<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\api\library;



class ConstAppProfileApiKey
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_APP_PROFILE_ENTITY_FACTORY = 'objAppProfileEntityFactory';
    const DATA_KEY_APP_PROFILE_ENTITY_FACTORY_EXEC_CONFIG = 'tabAppProfileEntityFactoryExecConfig';



    // Attribute configuration
    const ATTRIBUTE_KEY_ID = 'intAttrId';
    const ATTRIBUTE_KEY_DT_CREATE = 'attrDtCreate';
    const ATTRIBUTE_KEY_DT_UPDATE = 'attrDtUpdate';
    const ATTRIBUTE_KEY_NAME = 'strAttrName';
    const ATTRIBUTE_KEY_KEY = 'strAttrKey';
    const ATTRIBUTE_KEY_KEY_RESET = 'boolAttrKeyReset';
    const ATTRIBUTE_KEY_IS_BLOCKED = 'boolAttrIsBlocked';
    const ATTRIBUTE_KEY_APP_PROFILE = 'attrAppProfile';

    const ATTRIBUTE_ALIAS_ID = 'id';
    const ATTRIBUTE_ALIAS_DT_CREATE = 'dt-create';
    const ATTRIBUTE_ALIAS_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_ALIAS_NAME = 'name';
    const ATTRIBUTE_ALIAS_KEY = 'key';
    const ATTRIBUTE_ALIAS_KEY_RESET = 'key-reset';
    const ATTRIBUTE_ALIAS_IS_BLOCKED = 'is-blocked';
    const ATTRIBUTE_ALIAS_APP_PROFILE = 'app-profile';

    const ATTRIBUTE_NAME_SAVE_ID = 'id';
    const ATTRIBUTE_NAME_SAVE_DT_CREATE = 'dt-create';
    const ATTRIBUTE_NAME_SAVE_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_NAME_SAVE_NAME = 'name';
    const ATTRIBUTE_NAME_SAVE_KEY = 'key';
    const ATTRIBUTE_NAME_SAVE_KEY_RESET = 'key-reset';
    const ATTRIBUTE_NAME_SAVE_IS_BLOCKED = 'is-blocked';
    const ATTRIBUTE_NAME_SAVE_APP_PROFILE = 'profile';



    // Sub-action type configuration
    const SUB_ACTION_TYPE_ADMIN = 'admin';
    const SUB_ACTION_TYPE_PROFILE = 'profile';
    const SUB_ACTION_TYPE_PROFILE_CURRENT = 'profile_current';



    // Exception message constants
    const EXCEPT_MSG_APP_PROFILE_ENTITY_FACTORY_INVALID_FORMAT =
        'Following application profile entity factory "%1$s" invalid! It must be an application profile entity factory object.';
    const EXCEPT_MSG_APP_PROFILE_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the application profile entity factory execution configuration standard.';



}
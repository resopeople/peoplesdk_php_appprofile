<?php
/**
 * This class allows to define application profile attribute entity class.
 * Application profile attribute entity is a save attribute entity class,
 * can be used on application profile entity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\attribute\model;

use liberty_code\attribute_model\attribute\repository\model\SaveAttributeEntity;

use DateTime;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\attribute_model\attribute\library\ConstAttribute;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\model\entity\null_value\library\ToolBoxNullValue;
use people_sdk\app_profile\attribute\library\ConstAppProfileAttr;
use people_sdk\app_profile\attribute\library\ToolBoxAppProfileAttr;



/**
 * @property null|integer $intAttrId
 * @property null|string|DateTime $attrDtCreate
 * @property null|string|DateTime $attrDtUpdate
 * @property null|string $strAttrName
 * @property null|string $strAttrAlias @deprecated
 * @property null|string $strAttrDataType
 * @property null|boolean $boolAttrValueRequired
 * @property null|array $tabAttrListValue
 * @property null|array $tabAttrRuleConfig
 * @property null|string|mixed $attrDefaultValue
 * @property null|integer $intAttrOrder
 * @property null|boolean $boolAttrScopePrivateGetEnable
 * @property null|boolean $boolAttrScopePrivateUpdateEnable
 * @property null|boolean $boolAttrScopePublicGetEnable
 * @property null|boolean $boolAttrScopePublicExtendGetEnable
 */
class AppProfileAttrEntity extends SaveAttributeEntity
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkAttrAliasRequired()
    {
        // Return result
        return false;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Init var
        $tabUpdateConfig = array(
            // Select and update attribute id
            ConstAttribute::ATTRIBUTE_KEY_ID => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAppProfileAttr::ATTRIBUTE_NAME_SAVE_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute datetime create
            ConstAttribute::ATTRIBUTE_KEY_DT_CREATE => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAppProfileAttr::ATTRIBUTE_NAME_SAVE_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute datetime update
            ConstAttribute::ATTRIBUTE_KEY_DT_UPDATE => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAppProfileAttr::ATTRIBUTE_NAME_SAVE_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute name
            ConstAttribute::ATTRIBUTE_KEY_NAME => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAppProfileAttr::ATTRIBUTE_NAME_SAVE_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute data type
            ConstAttribute::ATTRIBUTE_KEY_DATA_TYPE => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAppProfileAttr::ATTRIBUTE_NAME_SAVE_DATA_TYPE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute value required
            ConstAttribute::ATTRIBUTE_KEY_VALUE_REQUIRED => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAppProfileAttr::ATTRIBUTE_NAME_SAVE_VALUE_REQUIRED,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute list value
            ConstAttribute::ATTRIBUTE_KEY_LIST_VALUE => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAppProfileAttr::ATTRIBUTE_NAME_SAVE_LIST_VALUE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute rule configuration
            ConstAttribute::ATTRIBUTE_KEY_RULE_CONFIG => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAppProfileAttr::ATTRIBUTE_NAME_SAVE_RULE_CONFIG,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute default value
            ConstAttribute::ATTRIBUTE_KEY_DEFAULT_VALUE => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAppProfileAttr::ATTRIBUTE_NAME_SAVE_DEFAULT_VALUE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Select and update attribute order
            ConstAttribute::ATTRIBUTE_KEY_ORDER => [
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAppProfileAttr::ATTRIBUTE_NAME_SAVE_ORDER,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],
        );
        // Select attribute config
        $result = array_filter(
            parent::getTabConfig(),
            function(array $tabAttrConfig) use ($tabUpdateConfig) {
                $strAttrKey = $tabAttrConfig[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY];

                return array_key_exists(
                    $strAttrKey,
                    $tabUpdateConfig
                );
            }
        );
        // Format attribute config
        $result = array_map(
            function(array $tabAttrConfig) use ($tabUpdateConfig) {
                $strAttrKey = $tabAttrConfig[ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY];
                $tabUpdateAttrConfig = $tabUpdateConfig[$strAttrKey];

                if(array_key_exists(ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE, $tabAttrConfig))
                {
                    unset($tabAttrConfig[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE]);
                }

                return array_merge(
                    $tabAttrConfig,
                    $tabUpdateAttrConfig
                );
            },
            $result
        );
        // Add attribute config
        $result = array_merge(
            $result,
            array(
                // Attribute private get enabled
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAppProfileAttr::ATTRIBUTE_KEY_SCOPE_PRIVATE_GET_ENABLE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAppProfileAttr::ATTRIBUTE_ALIAS_SCOPE_PRIVATE_GET_ENABLE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstAppProfileAttr::ATTRIBUTE_NAME_SAVE_SCOPE_PRIVATE_GET_ENABLE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute private update enabled
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAppProfileAttr::ATTRIBUTE_KEY_SCOPE_PRIVATE_UPDATE_ENABLE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAppProfileAttr::ATTRIBUTE_ALIAS_SCOPE_PRIVATE_UPDATE_ENABLE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstAppProfileAttr::ATTRIBUTE_NAME_SAVE_SCOPE_PRIVATE_UPDATE_ENABLE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute public get enabled
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAppProfileAttr::ATTRIBUTE_KEY_SCOPE_PUBLIC_GET_ENABLE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAppProfileAttr::ATTRIBUTE_ALIAS_SCOPE_PUBLIC_GET_ENABLE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstAppProfileAttr::ATTRIBUTE_NAME_SAVE_SCOPE_PUBLIC_GET_ENABLE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute public extended get enabled
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAppProfileAttr::ATTRIBUTE_KEY_SCOPE_PUBLIC_EXTEND_GET_ENABLE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAppProfileAttr::ATTRIBUTE_ALIAS_SCOPE_PUBLIC_EXTEND_GET_ENABLE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstAppProfileAttr::ATTRIBUTE_NAME_SAVE_SCOPE_PUBLIC_EXTEND_GET_ENABLE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ]
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );

        // Init var
        $tabRuleConfig = parent::getTabRuleConfig();
        $result = array_merge(
            $tabRuleConfig,
            array(
                ConstAttribute::ATTRIBUTE_KEY_ID => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => [
                                    'is_null',
                                    [
                                        'callable',
                                        [
                                            'valid_callable' => function() {return $this->checkIsNew();}
                                        ]
                                    ]
                                ],
                                'is-valid-id' => [
                                    [
                                        'type_numeric',
                                        ['integer_only_require' => true]
                                    ],
                                    [
                                        'compare_greater',
                                        [
                                            'compare_value' => 0,
                                            'equal_enable_require' => false
                                        ]
                                    ],
                                    [
                                        'callable',
                                        [
                                            'valid_callable' => function() {return (!$this->checkIsNew());}
                                        ]
                                    ]
                                ]
                            ],
                            'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                        ]
                    ]
                ],
                ConstAttribute::ATTRIBUTE_KEY_DT_CREATE => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-date' => $tabRuleConfig[ConstAttribute::ATTRIBUTE_KEY_DT_CREATE]
                            ],
                            'error_message_pattern' => '%1$s must be null or a date.'
                        ]
                    ]
                ],
                ConstAttribute::ATTRIBUTE_KEY_DT_UPDATE => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-date' => $tabRuleConfig[ConstAttribute::ATTRIBUTE_KEY_DT_UPDATE]
                            ],
                            'error_message_pattern' => '%1$s must be null or a date.'
                        ]
                    ]
                ],
                ConstAttribute::ATTRIBUTE_KEY_NAME => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-string' => $tabRuleConfig[ConstAttribute::ATTRIBUTE_KEY_NAME]
                            ],
                            'error_message_pattern' => '%1$s must be null or a string not empty.'
                        ]
                    ]
                ],
                ConstAttribute::ATTRIBUTE_KEY_DATA_TYPE => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-string' => $tabRuleConfig[ConstAttribute::ATTRIBUTE_KEY_DATA_TYPE]
                            ],
                            'error_message_pattern' => '%1$s must be null or a valid string.'
                        ]
                    ]
                ],
                ConstAttribute::ATTRIBUTE_KEY_VALUE_REQUIRED => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-string' => $tabRuleConfig[ConstAttribute::ATTRIBUTE_KEY_VALUE_REQUIRED]
                            ],
                            'error_message_pattern' => '%1$s must be null or a boolean.'
                        ]
                    ]
                ],
                ConstAttribute::ATTRIBUTE_KEY_LIST_VALUE => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-string' => $tabRuleConfig[ConstAttribute::ATTRIBUTE_KEY_LIST_VALUE]
                            ],
                            'error_message_pattern' => '%1$s must be null or an empty array or an array of null, string, numeric or boolean values.'
                        ]
                    ]
                ],
                ConstAttribute::ATTRIBUTE_KEY_RULE_CONFIG => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-string' => $tabRuleConfig[ConstAttribute::ATTRIBUTE_KEY_RULE_CONFIG]
                            ],
                            'error_message_pattern' => '%1$s must be a valid rule configuration array.'
                        ]
                    ]
                ],
                ConstAttribute::ATTRIBUTE_KEY_ORDER => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-string' => $tabRuleConfig[ConstAttribute::ATTRIBUTE_KEY_ORDER]
                            ],
                            'error_message_pattern' => '%1$s must be null or a positive integer.'
                        ]
                    ]
                ],
                ConstAppProfileAttr::ATTRIBUTE_KEY_SCOPE_PRIVATE_GET_ENABLE => $tabRuleConfigValidBoolean,
                ConstAppProfileAttr::ATTRIBUTE_KEY_SCOPE_PRIVATE_UPDATE_ENABLE => $tabRuleConfigValidBoolean,
                ConstAppProfileAttr::ATTRIBUTE_KEY_SCOPE_PUBLIC_GET_ENABLE => $tabRuleConfigValidBoolean,
                ConstAppProfileAttr::ATTRIBUTE_KEY_SCOPE_PUBLIC_EXTEND_GET_ENABLE => $tabRuleConfigValidBoolean
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstAttribute::ATTRIBUTE_KEY_ID:
            case ConstAttribute::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAttribute::ATTRIBUTE_KEY_DT_UPDATE:
            case ConstAttribute::ATTRIBUTE_KEY_VALUE_REQUIRED:
            case ConstAttribute::ATTRIBUTE_KEY_RULE_CONFIG:
            case ConstAttribute::ATTRIBUTE_KEY_ORDER:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        parent::getAttributeValueFormatSet($strKey, $value)
                );
                break;

            case ConstAttribute::ATTRIBUTE_KEY_NAME:
            case ConstAttribute::ATTRIBUTE_KEY_DATA_TYPE:
            case ConstAttribute::ATTRIBUTE_KEY_LIST_VALUE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstAppProfileAttr::ATTRIBUTE_KEY_SCOPE_PRIVATE_GET_ENABLE:
            case ConstAppProfileAttr::ATTRIBUTE_KEY_SCOPE_PRIVATE_UPDATE_ENABLE:
            case ConstAppProfileAttr::ATTRIBUTE_KEY_SCOPE_PUBLIC_GET_ENABLE:
            case ConstAppProfileAttr::ATTRIBUTE_KEY_SCOPE_PUBLIC_EXTEND_GET_ENABLE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) === 1) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstAttribute::ATTRIBUTE_KEY_ID:
            case ConstAttribute::ATTRIBUTE_KEY_VALUE_REQUIRED:
            case ConstAttribute::ATTRIBUTE_KEY_LIST_VALUE :
            case ConstAttribute::ATTRIBUTE_KEY_RULE_CONFIG :
            case ConstAttribute::ATTRIBUTE_KEY_ORDER:
            case ConstAppProfileAttr::ATTRIBUTE_KEY_SCOPE_PRIVATE_GET_ENABLE:
            case ConstAppProfileAttr::ATTRIBUTE_KEY_SCOPE_PRIVATE_UPDATE_ENABLE:
            case ConstAppProfileAttr::ATTRIBUTE_KEY_SCOPE_PUBLIC_GET_ENABLE:
            case ConstAppProfileAttr::ATTRIBUTE_KEY_SCOPE_PUBLIC_EXTEND_GET_ENABLE:
                $result = $value;
                break;

            case ConstAttribute::ATTRIBUTE_KEY_DEFAULT_VALUE:
                $result = ToolBoxNullValue::getAttributeValueSaveFormatGet($value);
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Format by attribute
        switch($strKey)
        {
            case ConstAttribute::ATTRIBUTE_KEY_ID:
            case ConstAttribute::ATTRIBUTE_KEY_VALUE_REQUIRED:
            case ConstAttribute::ATTRIBUTE_KEY_LIST_VALUE :
            case ConstAttribute::ATTRIBUTE_KEY_RULE_CONFIG :
            case ConstAttribute::ATTRIBUTE_KEY_ORDER:
            case ConstAppProfileAttr::ATTRIBUTE_KEY_SCOPE_PRIVATE_GET_ENABLE:
            case ConstAppProfileAttr::ATTRIBUTE_KEY_SCOPE_PRIVATE_UPDATE_ENABLE:
            case ConstAppProfileAttr::ATTRIBUTE_KEY_SCOPE_PUBLIC_GET_ENABLE:
            case ConstAppProfileAttr::ATTRIBUTE_KEY_SCOPE_PUBLIC_EXTEND_GET_ENABLE:
                $result = $value;
                break;

            case ConstAttribute::ATTRIBUTE_KEY_DEFAULT_VALUE:
                $result = ToolBoxNullValue::getAttributeValueSaveFormatSet($value);
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function getStrAttributeKey()
    {
        // Init var
        $strAttributeName = $this->getAttributeValue(ConstAttribute::ATTRIBUTE_KEY_NAME);
        $result = sprintf(
            ConstAttribute::CONF_ATTRIBUTE_KEY_PATTERN,
            ToolBoxAppProfileAttr::getStrAttributeKey($strAttributeName)
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabEntityAttrConfig()
    {
        // Init var
        $result = array_merge(
            parent::getTabEntityAttrConfig(),
            array(
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    $this->getAttributeValue(ConstAttribute::ATTRIBUTE_KEY_NAME),
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
            )
        );
        unset($result[ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE]);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function getTabEntityAttrRuleConfig()
    {
        // Return result
        return array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-value' => parent::getTabEntityAttrRuleConfig()
                    ],
                    'error_message_pattern' => '%1$s must be null or a valid value.'
                ]
            ]
        );
    }



    /**
     * @inheritdoc
     */
    public function getEntityAttrValueSaveFormatGet($value)
    {
        // Return result
        return ToolBoxNullValue::getAttributeValueSaveFormatGet(
            parent::getEntityAttrValueSaveFormatGet($value)
        );
    }



    /**
     * @inheritdoc
     */
    public function getEntityAttrValueSaveFormatSet($value)
    {
        // Return result
        return parent::getEntityAttrValueSaveFormatSet(
            ToolBoxNullValue::getAttributeValueSaveFormatSet($value)
        );
    }



}
<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\app\library;



class ConstAppProfile
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_APP_PROFILE_ATTR_VALUE_ENTITY_FACTORY = 'objAppProfileAttrValueEntityFactory';
    const DATA_KEY_APP_PROFILE_ATTR_VALUE_ENTITY_FACTORY_EXEC_CONFIG = 'tabAppProfileAttrValueEntityFactoryExecConfig';



    // Attribute configuration
    const ATTRIBUTE_KEY_ID = 'intAttrId';
    const ATTRIBUTE_KEY_DT_CREATE = 'attrDtCreate';
    const ATTRIBUTE_KEY_DT_UPDATE = 'attrDtUpdate';
    const ATTRIBUTE_KEY_NAME = 'strAttrName';
    const ATTRIBUTE_KEY_SECRET = 'strAttrSecret';
    const ATTRIBUTE_KEY_SECRET_RESET = 'boolAttrSecretReset';
    const ATTRIBUTE_KEY_SECRET_VERIFY = 'strAttrSecretVerify';
    const ATTRIBUTE_KEY_SECRET_NEW = 'strAttrSecretNew';
    const ATTRIBUTE_KEY_IS_BLOCKED = 'boolAttrIsBlocked';
    const ATTRIBUTE_KEY_IMG_SRC_NAME = 'strAttrImgSrcName';
    const ATTRIBUTE_KEY_IMG_TYPE = 'strAttrImgType';
    const ATTRIBUTE_KEY_IMG_SIZE = 'intAttrImgSize';
    const ATTRIBUTE_KEY_IMAGE = 'attrImage';
    const ATTRIBUTE_KEY_APP_PROFILE_ATTR_VALUE_ENTITY = 'objAttrAppProfileAttrValueEntity';

    const ATTRIBUTE_ALIAS_ID = 'id';
    const ATTRIBUTE_ALIAS_DT_CREATE = 'dt-create';
    const ATTRIBUTE_ALIAS_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_ALIAS_NAME = 'name';
    const ATTRIBUTE_ALIAS_SECRET = 'secret';
    const ATTRIBUTE_ALIAS_SECRET_RESET = 'secret-reset';
    const ATTRIBUTE_ALIAS_SECRET_VERIFY = 'secret-verify';
    const ATTRIBUTE_ALIAS_SECRET_NEW = 'secret-new';
    const ATTRIBUTE_ALIAS_IS_BLOCKED = 'is-blocked';
    const ATTRIBUTE_ALIAS_IMG_SRC_NAME = 'image-source-name';
    const ATTRIBUTE_ALIAS_IMG_TYPE = 'image-type';
    const ATTRIBUTE_ALIAS_IMG_SIZE = 'image-size';
    const ATTRIBUTE_ALIAS_IMAGE = 'image';
    const ATTRIBUTE_ALIAS_APP_PROFILE_ATTR_VALUE_ENTITY = 'attribute';

    const ATTRIBUTE_NAME_SAVE_ID = 'id';
    const ATTRIBUTE_NAME_SAVE_DT_CREATE = 'dt-create';
    const ATTRIBUTE_NAME_SAVE_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_NAME_SAVE_NAME = 'name';
    const ATTRIBUTE_NAME_SAVE_SECRET = 'secret';
    const ATTRIBUTE_NAME_SAVE_SECRET_RESET = 'secret-reset';
    const ATTRIBUTE_NAME_SAVE_SECRET_VERIFY = 'secret-verify';
    const ATTRIBUTE_NAME_SAVE_SECRET_NEW = 'secret-new';
    const ATTRIBUTE_NAME_SAVE_IS_BLOCKED = 'is-blocked';
    const ATTRIBUTE_NAME_SAVE_IMG_SRC_NAME = 'image-source-name';
    const ATTRIBUTE_NAME_SAVE_IMG_TYPE = 'image-type';
    const ATTRIBUTE_NAME_SAVE_IMG_SIZE = 'image-size';
    const ATTRIBUTE_NAME_SAVE_IMAGE = 'image';
    const ATTRIBUTE_NAME_SAVE_APP_PROFILE_ATTR_VALUE_ENTITY = 'attribute';



    // Sub-action type configuration
    const SUB_ACTION_TYPE_ADMIN = 'admin';
    const SUB_ACTION_TYPE_ADMIN_ROLE = 'admin_role';
    const SUB_ACTION_TYPE_PROFILE = 'profile';
    const SUB_ACTION_TYPE_PROFILE_ROLE = 'profile_role';



    // Exception message constants
    const EXCEPT_MSG_APP_PROFILE_ATTR_VALUE_ENTITY_INVALID_FORMAT =
        'Following application profile attribute value entity factory "%1$s" invalid! It must be an application profile attribute value entity factory object.';
    const EXCEPT_MSG_APP_PROFILE_ATTR_VALUE_ENTITY_FACTORY_EXEC_CONFIG_INVALID_FORMAT =
        'Following config "%1$s" invalid! 
        The config must be an array and following the application profile attribute value entity factory execution configuration standard.';



}
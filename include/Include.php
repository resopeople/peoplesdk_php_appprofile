<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/attribute/library/ConstAppProfileAttr.php');
include($strRootPath . '/src/attribute/library/ToolBoxAppProfileAttr.php');
include($strRootPath . '/src/attribute/library/ToolBoxAppProfileAttrRepository.php');
include($strRootPath . '/src/attribute/model/AppProfileAttrEntity.php');
include($strRootPath . '/src/attribute/model/AppProfileAttrEntityCollection.php');
include($strRootPath . '/src/attribute/model/AppProfileAttrEntityFactory.php');
include($strRootPath . '/src/attribute/model/repository/AppProfileAttrEntitySimpleRepository.php');
include($strRootPath . '/src/attribute/model/repository/AppProfileAttrEntitySimpleCollectionRepository.php');
include($strRootPath . '/src/attribute/model/repository/AppProfileAttrEntityMultiRepository.php');
include($strRootPath . '/src/attribute/model/repository/AppProfileAttrEntityMultiCollectionRepository.php');

include($strRootPath . '/src/attribute/browser/library/ConstAppProfileAttrBrowser.php');
include($strRootPath . '/src/attribute/browser/model/AppProfileAttrBrowserEntity.php');

include($strRootPath . '/src/attribute/provider/model/AppProfileAttrProvider.php');

include($strRootPath . '/src/app/requisition/request/info/library/ConstAppProfileSndInfo.php');
include($strRootPath . '/src/app/requisition/request/info/library/ToolBoxAppProfileSndInfo.php');

include($strRootPath . '/src/app/attribute/value/library/ConstAppProfileAttrValue.php');
include($strRootPath . '/src/app/attribute/value/exception/AppProfileAttrProviderInvalidFormatException.php');
include($strRootPath . '/src/app/attribute/value/model/AppProfileAttrValueEntity.php');
include($strRootPath . '/src/app/attribute/value/model/AppProfileAttrValueEntityFactory.php');

include($strRootPath . '/src/app/library/ConstAppProfile.php');
include($strRootPath . '/src/app/exception/AppProfileAttrValueEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/app/exception/AppProfileAttrValueEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/app/model/AppProfileEntity.php');
include($strRootPath . '/src/app/model/AppProfileEntityCollection.php');
include($strRootPath . '/src/app/model/AppProfileEntityFactory.php');
include($strRootPath . '/src/app/model/repository/AppProfileEntitySimpleRepository.php');
include($strRootPath . '/src/app/model/repository/AppProfileEntityMultiRepository.php');
include($strRootPath . '/src/app/model/repository/AppProfileEntityMultiCollectionRepository.php');

include($strRootPath . '/src/app/browser/library/ConstAppProfileBrowser.php');
include($strRootPath . '/src/app/browser/model/AppProfileBrowserEntity.php');

include($strRootPath . '/src/api/requisition/request/info/library/ToolBoxAppProfileApiKeySndInfo.php');

include($strRootPath . '/src/api/library/ConstAppProfileApiKey.php');
include($strRootPath . '/src/api/exception/AppProfileEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/api/exception/AppProfileEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/api/model/AppProfileApiKeyEntity.php');
include($strRootPath . '/src/api/model/AppProfileApiKeyEntityCollection.php');
include($strRootPath . '/src/api/model/AppProfileApiKeyEntityFactory.php');
include($strRootPath . '/src/api/model/repository/AppProfileApiKeyEntitySimpleRepository.php');
include($strRootPath . '/src/api/model/repository/AppProfileApiKeyEntityMultiRepository.php');
include($strRootPath . '/src/api/model/repository/AppProfileApiKeyEntityMultiCollectionRepository.php');

include($strRootPath . '/src/api/browser/library/ConstAppProfileApiKeyBrowser.php');
include($strRootPath . '/src/api/browser/model/AppProfileApiKeyBrowserEntity.php');

include($strRootPath . '/src/token/requisition/request/info/library/ToolBoxAppProfileTokenKeySndInfo.php');

include($strRootPath . '/src/token/library/ConstAppProfileTokenKey.php');
include($strRootPath . '/src/token/exception/AppProfileEntityFactoryInvalidFormatException.php');
include($strRootPath . '/src/token/exception/AppProfileEntityFactoryExecConfigInvalidFormatException.php');
include($strRootPath . '/src/token/model/AppProfileTokenKeyEntity.php');
include($strRootPath . '/src/token/model/AppProfileTokenKeyEntityCollection.php');
include($strRootPath . '/src/token/model/AppProfileTokenKeyEntityFactory.php');
include($strRootPath . '/src/token/model/repository/AppProfileTokenKeyEntitySimpleRepository.php');
include($strRootPath . '/src/token/model/repository/AppProfileTokenKeyEntityMultiRepository.php');
include($strRootPath . '/src/token/model/repository/AppProfileTokenKeyEntityMultiCollectionRepository.php');

include($strRootPath . '/src/token/browser/library/ConstAppProfileTokenKeyBrowser.php');
include($strRootPath . '/src/token/browser/model/AppProfileTokenKeyBrowserEntity.php');

include($strRootPath . '/src/requisition/request/info/factory/library/ConstAppProfileConfigSndInfoFactory.php');
include($strRootPath . '/src/requisition/request/info/factory/exception/ConfigInvalidFormatException.php');
include($strRootPath . '/src/requisition/request/info/factory/model/AppProfileConfigSndInfoFactory.php');
<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\attribute\library;



class ConstAppProfileAttr
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Attribute configuration
    const ATTRIBUTE_KEY_SCOPE_PRIVATE_GET_ENABLE = 'boolAttrScopePrivateGetEnable';
    const ATTRIBUTE_KEY_SCOPE_PRIVATE_UPDATE_ENABLE = 'boolAttrScopePrivateUpdateEnable';
    const ATTRIBUTE_KEY_SCOPE_PUBLIC_GET_ENABLE = 'boolAttrScopePublicGetEnable';
    const ATTRIBUTE_KEY_SCOPE_PUBLIC_EXTEND_GET_ENABLE = 'boolAttrScopePublicExtendGetEnable';

    const ATTRIBUTE_ALIAS_SCOPE_PRIVATE_GET_ENABLE = 'scope-private-get-enable';
    const ATTRIBUTE_ALIAS_SCOPE_PRIVATE_UPDATE_ENABLE = 'scope-private-update-enable';
    const ATTRIBUTE_ALIAS_SCOPE_PUBLIC_GET_ENABLE = 'scope-public-get-enable';
    const ATTRIBUTE_ALIAS_SCOPE_PUBLIC_EXTEND_GET_ENABLE = 'scope-public-extend-get-enable';

    const ATTRIBUTE_NAME_SAVE_ID = 'id';
    const ATTRIBUTE_NAME_SAVE_DT_CREATE = 'dt-create';
    const ATTRIBUTE_NAME_SAVE_DT_UPDATE = 'dt-update';
    const ATTRIBUTE_NAME_SAVE_NAME = 'name';
    const ATTRIBUTE_NAME_SAVE_DATA_TYPE = 'data-type';
    const ATTRIBUTE_NAME_SAVE_VALUE_REQUIRED = 'value-required';
    const ATTRIBUTE_NAME_SAVE_LIST_VALUE = 'list-value';
    const ATTRIBUTE_NAME_SAVE_RULE_CONFIG = 'rule-config';
    const ATTRIBUTE_NAME_SAVE_DEFAULT_VALUE = 'default-value';
    const ATTRIBUTE_NAME_SAVE_ORDER = 'order';
    const ATTRIBUTE_NAME_SAVE_SCOPE_PRIVATE_GET_ENABLE = 'scope-private-get-enable';
    const ATTRIBUTE_NAME_SAVE_SCOPE_PRIVATE_UPDATE_ENABLE = 'scope-private-update-enable';
    const ATTRIBUTE_NAME_SAVE_SCOPE_PUBLIC_GET_ENABLE = 'scope-public-get-enable';
    const ATTRIBUTE_NAME_SAVE_SCOPE_PUBLIC_EXTEND_GET_ENABLE = 'scope-public-extend-get-enable';



    // Sub-action type configuration
    const SUB_ACTION_TYPE_ADMIN = 'admin';
    const SUB_ACTION_TYPE_SCHEMA = 'schema';
    const SUB_ACTION_TYPE_SCHEMA_PRIVATE_GET = 'schema_private_get';
    const SUB_ACTION_TYPE_SCHEMA_PRIVATE_UPDATE = 'schema_private_update';
    const SUB_ACTION_TYPE_SCHEMA_PUBLIC_GET = 'schema_public_get';
    const SUB_ACTION_TYPE_SCHEMA_PUBLIC_EXTEND_GET = 'schema_public_extend_get';



    // Exception message constants
    const EXCEPT_MSG_ATTRIBUTE_VALUE_IS_NULL = 'Following attribute value "%1$s" is null!';



}
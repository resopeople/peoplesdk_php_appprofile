<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\app\attribute\value\library;



class ConstAppProfileAttrValue
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Data constants
    const DATA_KEY_DEFAULT_APP_PROFILE_ATTR_PROVIDER = 'objAppProfileAttrProvider';



    // Exception message constants
    const EXCEPT_MSG_APP_PROFILE_ATTR_PROVIDER_INVALID_FORMAT =
        'Following application profile attribute provider "%1$s" invalid! It must be an application profile attribute provider object.';



}
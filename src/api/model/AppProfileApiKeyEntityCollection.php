<?php
/**
 * This class allows to define application profile API key entity collection class.
 * key => AppProfileApiKeyEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\api\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\app_profile\api\model\AppProfileApiKeyEntity;



/**
 * @method null|AppProfileApiKeyEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(AppProfileApiKeyEntity $objEntity) @inheritdoc
 */
class AppProfileApiKeyEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return AppProfileApiKeyEntity::class;
    }



}
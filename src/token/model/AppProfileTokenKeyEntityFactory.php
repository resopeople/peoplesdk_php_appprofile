<?php
/**
 * This class allows to define application profile token key entity factory class.
 * Application profile token key entity factory allows to provide new application profile token key entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\token\model;

use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\app_profile\app\model\AppProfileEntityFactory;
use people_sdk\app_profile\token\library\ConstAppProfileTokenKey;
use people_sdk\app_profile\token\exception\AppProfileEntityFactoryInvalidFormatException;
use people_sdk\app_profile\token\exception\AppProfileEntityFactoryExecConfigInvalidFormatException;
use people_sdk\app_profile\token\model\AppProfileTokenKeyEntity;



/**
 * @method null|AppProfileEntityFactory getObjAppProfileEntityFactory() Get application profile entity factory object.
 * @method null|array getTabAppProfileEntityFactoryExecConfig() Get application profile entity factory execution configuration array.
 * @method AppProfileTokenKeyEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method void setObjAppProfileEntityFactory(null|AppProfileEntityFactory $objAppProfileEntityFactory) Set application profile entity factory object.
 * @method void setTabAppProfileEntityFactoryExecConfig(null|array $tabAppProfileEntityFactoryExecConfig) Set application profile entity factory execution configuration array.
 */
class AppProfileTokenKeyEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|AppProfileEntityFactory $objAppProfileEntityFactory = null
     * @param null|array $tabAppProfileEntityFactoryExecConfig = null
     */
    public function __construct(
        ProviderInterface $objProvider,
        EntityCollectionInterface $objEntityCollection = null,
        AppProfileEntityFactory $objAppProfileEntityFactory = null,
        array $tabAppProfileEntityFactoryExecConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objProvider,
            $objEntityCollection
        );

        // Init application profile entity factory
        $this->setObjAppProfileEntityFactory($objAppProfileEntityFactory);

        // Init application profile entity factory execution config
        $this->setTabAppProfileEntityFactoryExecConfig($tabAppProfileEntityFactoryExecConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstAppProfileTokenKey::DATA_KEY_APP_PROFILE_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstAppProfileTokenKey::DATA_KEY_APP_PROFILE_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstAppProfileTokenKey::DATA_KEY_APP_PROFILE_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstAppProfileTokenKey::DATA_KEY_APP_PROFILE_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstAppProfileTokenKey::DATA_KEY_APP_PROFILE_ENTITY_FACTORY,
            ConstAppProfileTokenKey::DATA_KEY_APP_PROFILE_ENTITY_FACTORY_EXEC_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstAppProfileTokenKey::DATA_KEY_APP_PROFILE_ENTITY_FACTORY:
                    AppProfileEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstAppProfileTokenKey::DATA_KEY_APP_PROFILE_ENTITY_FACTORY_EXEC_CONFIG:
                    AppProfileEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => AppProfileTokenKeyEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => ConstAppProfileTokenKey::ATTRIBUTE_KEY_ID
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNewEngine(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $result = new AppProfileTokenKeyEntity(
            array(),
            $objValidator,
            $objDateTimeFactory,
            $this->getObjAppProfileEntityFactory(),
            $this->getTabAppProfileEntityFactoryExecConfig()
        );

        // Return result
        return $result;
    }



}
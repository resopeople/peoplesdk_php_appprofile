<?php
/**
 * This class allows to define application profile token key entity collection class.
 * key => AppProfileTokenKeyEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\token\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\app_profile\token\model\AppProfileTokenKeyEntity;



/**
 * @method null|AppProfileTokenKeyEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(AppProfileTokenKeyEntity $objEntity) @inheritdoc
 */
class AppProfileTokenKeyEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return AppProfileTokenKeyEntity::class;
    }



}
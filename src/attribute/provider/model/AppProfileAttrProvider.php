<?php
/**
 * Description :
 * This class allows to define application profile attribute provider class.
 * Application profile attribute provider is standard attribute provider,
 * uses application profile attribute entity collection,
 * to provide attribute information, for application profile entity.
 *
 * Application profile attribute provider uses the following specified configuration:
 * [
 *     Standard attribute provider configuration
 * ]
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\attribute\provider\model;

use liberty_code\handle_model\attribute\provider\standard\model\StandardAttrProvider;

use liberty_code\cache\repository\api\RepositoryInterface;
use people_sdk\app_profile\attribute\model\AppProfileAttrEntityCollection;



/**
 * @method AppProfileAttrEntityCollection getObjAttributeCollection() @inheritdoc
 * @method void setObjAttributeCollection(AppProfileAttrEntityCollection $objAttributeCollection) @inheritdoc
 */
class AppProfileAttrProvider extends StandardAttrProvider
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param AppProfileAttrEntityCollection $objAttributeCollection
     */
    public function __construct(
        AppProfileAttrEntityCollection $objAttributeCollection,
        array $tabConfig = null,
        RepositoryInterface $objCacheRepo = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objAttributeCollection,
            $tabConfig,
            $objCacheRepo
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     *
     * @return string
     */
    protected function getStrFixAttributeCollectionClassPath()
    {
        // Return result
        return AppProfileAttrEntityCollection::class;
    }



}
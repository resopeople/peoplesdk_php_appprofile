<?php
/**
 * This class allows to define application profile entity class.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\app\model;

use people_sdk\role\profile\model\RoleProfileEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\file\file\api\FileInterface;
use liberty_code\file\file\name\api\NameFileInterface;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\model\entity\null_value\library\ConstNullValue;
use people_sdk\library\model\entity\null_value\library\ToolBoxNullValue;
use people_sdk\role\permission\subject\model\SubjPermEntityFactory;
use people_sdk\role\role\model\RoleEntityFactory;
use people_sdk\app_profile\app\attribute\value\model\AppProfileAttrValueEntity;
use people_sdk\app_profile\app\attribute\value\model\AppProfileAttrValueEntityFactory;
use people_sdk\app_profile\app\library\ConstAppProfile;



/**
 * @property null|integer $intAttrId
 * @property null|string|DateTime $attrDtCreate
 * @property null|string|DateTime $attrDtUpdate
 * @property null|string $strAttrName
 * @property null|string $strAttrSecret
 * @property null|boolean $boolAttrSecretReset
 * @property null|string $strAttrSecretVerify
 * @property null|string $strAttrSecretNew
 * @property null|boolean $boolAttrIsBlocked
 * @property null|string $strAttrImgSrcName
 * @property null|string $strAttrImgType : image mime type
 * @property null|integer $intAttrImgSize : image size in bytes
 * @property null|string|FileInterface $attrImage : image URL or file
 * @property null|AppProfileAttrValueEntity $objAttrAppProfileAttrValueEntity
 */
class AppProfileEntity extends RoleProfileEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;



    /**
     * DI: Application profile attribute value entity factory instance.
     * @var null|AppProfileAttrValueEntityFactory
     */
    protected $objAppProfileAttrValueEntityFactory;



    /**
     * DI: Application profile attribute value entity factory execution configuration.
     * @var null|array
     */
    protected $tabAppProfileAttrValueEntityFactoryExecConfig;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|DateTimeFactoryInterface $objDateTimeFactory = null
     * @param null|AppProfileAttrValueEntityFactory $objAppProfileAttrValueEntityFactory = null
     * @param null|array $tabAppProfileAttrValueEntityFactoryExecConfig = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null,
        AppProfileAttrValueEntityFactory $objAppProfileAttrValueEntityFactory = null,
        SubjPermEntityFactory $objSubjPermEntityFactory = null,
        RoleEntityFactory $objRoleEntityFactory = null,
        array $tabAppProfileAttrValueEntityFactoryExecConfig = null,
        array $tabSubjPermEntityFactoryExecConfig = null,
        array $tabRoleEntityFactoryExecConfig = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;
        $this->objAppProfileAttrValueEntityFactory = $objAppProfileAttrValueEntityFactory;
        $this->tabAppProfileAttrValueEntityFactoryExecConfig = $tabAppProfileAttrValueEntityFactoryExecConfig;

        // Call parent constructor
        parent::__construct(
            $tabValue,
            $objValidator,
            $objSubjPermEntityFactory,
            $objRoleEntityFactory,
            $tabSubjPermEntityFactoryExecConfig,
            $tabRoleEntityFactoryExecConfig
        );
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function checkAttributeSaveGetEnabled($strKey)
    {
        // Init var
        $value = $this->getAttributeValue($strKey, false);

        // Check by attribute
        switch($strKey)
        {
            case ConstAppProfile::ATTRIBUTE_KEY_IMAGE:
                $result = (
                    parent::checkAttributeSaveGetEnabled($strKey) &&
                    (
                        (!is_string($value)) ||
                        ($value === ConstNullValue::NULL_VALUE)
                    )
                );
                break;

            default:
                $result = parent::checkAttributeSaveGetEnabled($strKey);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getStrBaseKey()
    {
        // Init var
        $intId = $this->getAttributeValue(ConstAppProfile::ATTRIBUTE_KEY_ID);
        $result = (
            (is_int($intId) && ($intId > 0)) ?
                strval($intId) :
                parent::getStrBaseKey()
        );

        // Return result
        return $result;
    }



    /**
     * Get default attribute application profile attribute value entity,
     * used on attribute configuration.
     *
     * @return null|AppProfileAttrValueEntity
     */
    protected function getObjDefaultAttrAppProfileAttrValueEntity()
    {
        // Return result
        return (
            (!is_null($this->objAppProfileAttrValueEntityFactory)) ?
                $this
                    ->objAppProfileAttrValueEntityFactory
                    ->getObjEntity(
                        array(),
                        $this->tabAppProfileAttrValueEntityFactoryExecConfig
                    ) :
                null
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array_merge(
            parent::getTabConfig(),
            array(
                // Attribute id
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAppProfile::ATTRIBUTE_KEY_ID,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAppProfile::ATTRIBUTE_ALIAS_ID,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute datetime create
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAppProfile::ATTRIBUTE_KEY_DT_CREATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAppProfile::ATTRIBUTE_ALIAS_DT_CREATE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstAppProfile::ATTRIBUTE_NAME_SAVE_DT_CREATE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute datetime update
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAppProfile::ATTRIBUTE_KEY_DT_UPDATE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAppProfile::ATTRIBUTE_ALIAS_DT_UPDATE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstAppProfile::ATTRIBUTE_NAME_SAVE_DT_UPDATE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAppProfile::ATTRIBUTE_KEY_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAppProfile::ATTRIBUTE_ALIAS_NAME,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstAppProfile::ATTRIBUTE_NAME_SAVE_NAME,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute secret
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAppProfile::ATTRIBUTE_KEY_SECRET,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAppProfile::ATTRIBUTE_ALIAS_SECRET,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstAppProfile::ATTRIBUTE_NAME_SAVE_SECRET,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE =>null
                ],

                // Attribute secret reset
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAppProfile::ATTRIBUTE_KEY_SECRET_RESET,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAppProfile::ATTRIBUTE_ALIAS_SECRET_RESET,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstAppProfile::ATTRIBUTE_NAME_SAVE_SECRET_RESET,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => false,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute secret verify
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAppProfile::ATTRIBUTE_KEY_SECRET_VERIFY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAppProfile::ATTRIBUTE_ALIAS_SECRET_VERIFY,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstAppProfile::ATTRIBUTE_NAME_SAVE_SECRET_VERIFY,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => false,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute secret new
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAppProfile::ATTRIBUTE_KEY_SECRET_NEW,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAppProfile::ATTRIBUTE_ALIAS_SECRET_NEW,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstAppProfile::ATTRIBUTE_NAME_SAVE_SECRET_NEW,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => false,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute is blocked
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAppProfile::ATTRIBUTE_KEY_IS_BLOCKED,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAppProfile::ATTRIBUTE_ALIAS_IS_BLOCKED,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstAppProfile::ATTRIBUTE_NAME_SAVE_IS_BLOCKED,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute image source name
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAppProfile::ATTRIBUTE_KEY_IMG_SRC_NAME,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAppProfile::ATTRIBUTE_ALIAS_IMG_SRC_NAME,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstAppProfile::ATTRIBUTE_NAME_SAVE_IMG_SRC_NAME,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute image type
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAppProfile::ATTRIBUTE_KEY_IMG_TYPE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAppProfile::ATTRIBUTE_ALIAS_IMG_TYPE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstAppProfile::ATTRIBUTE_NAME_SAVE_IMG_TYPE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute image size
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAppProfile::ATTRIBUTE_KEY_IMG_SIZE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAppProfile::ATTRIBUTE_ALIAS_IMG_SIZE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstAppProfile::ATTRIBUTE_NAME_SAVE_IMG_SIZE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute image
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAppProfile::ATTRIBUTE_KEY_IMAGE,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAppProfile::ATTRIBUTE_ALIAS_IMAGE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstAppProfile::ATTRIBUTE_NAME_SAVE_IMAGE,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
                ],

                // Attribute application profile attribute value entity
                [
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                        ConstAppProfile::ATTRIBUTE_KEY_APP_PROFILE_ATTR_VALUE_ENTITY,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                        ConstAppProfile::ATTRIBUTE_ALIAS_APP_PROFILE_ATTR_VALUE_ENTITY,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                        ConstAppProfile::ATTRIBUTE_NAME_SAVE_APP_PROFILE_ATTR_VALUE_ENTITY,
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                        [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                    ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                    ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE =>
                        $this->getObjDefaultAttrAppProfileAttrValueEntity()
                ]
            )
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $getTabRuleConfigValidString = function($boolValueNullRequire = false)
        {
            $boolValueNullRequire = (is_bool($boolValueNullRequire) ? $boolValueNullRequire : true);
            $result = array(
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-string' => [
                                'type_string',
                                [
                                    'sub_rule_not',
                                    [
                                        'rule_config' => ['is_empty']
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null or a string not empty'
                    ]
                ]
            );

            if($boolValueNullRequire)
            {
                $result[0][1]['rule_config']['is-valid-null'] = array(
                    [
                        'compare_equal',
                        ['compare_value' => ConstNullValue::NULL_VALUE]
                    ]
                );
            }

            return $result;
        };
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );

        // Init var
        $result = parent::getTabRuleConfig();
        $result = array_merge(
            $result,
            array(
                ConstAppProfile::ATTRIBUTE_KEY_ID => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => [
                                    'is_null',
                                    [
                                        'callable',
                                        [
                                            'valid_callable' => function() {return $this->checkIsNew();}
                                        ]
                                    ]
                                ],
                                'is-valid-id' => [
                                    [
                                        'type_numeric',
                                        ['integer_only_require' => true]
                                    ],
                                    [
                                        'compare_greater',
                                        [
                                            'compare_value' => 0,
                                            'equal_enable_require' => false
                                        ]
                                    ],
                                    [
                                        'callable',
                                        [
                                            'valid_callable' => function() {return (!$this->checkIsNew());}
                                        ]
                                    ]
                                ]
                            ],
                            'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                        ]
                    ]
                ],
                ConstAppProfile::ATTRIBUTE_KEY_DT_CREATE => $tabRuleConfigValidDate,
                ConstAppProfile::ATTRIBUTE_KEY_DT_UPDATE => $tabRuleConfigValidDate,
                ConstAppProfile::ATTRIBUTE_KEY_NAME => $getTabRuleConfigValidString(),
                ConstAppProfile::ATTRIBUTE_KEY_SECRET => $getTabRuleConfigValidString(),
                ConstAppProfile::ATTRIBUTE_KEY_SECRET_RESET => $tabRuleConfigValidBoolean,
                ConstAppProfile::ATTRIBUTE_KEY_SECRET_VERIFY => $getTabRuleConfigValidString(),
                ConstAppProfile::ATTRIBUTE_KEY_SECRET_NEW => $getTabRuleConfigValidString(),
                ConstAppProfile::ATTRIBUTE_KEY_IS_BLOCKED => $tabRuleConfigValidBoolean,
                ConstAppProfile::ATTRIBUTE_KEY_IMG_SRC_NAME => $getTabRuleConfigValidString(true),
                ConstAppProfile::ATTRIBUTE_KEY_IMG_TYPE => $getTabRuleConfigValidString(true),
                ConstAppProfile::ATTRIBUTE_KEY_IMG_SIZE => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-null' => [
                                    [
                                        'compare_equal',
                                        ['compare_value' => ConstNullValue::NULL_VALUE]
                                    ]
                                ],
                                'is-valid-integer' => [
                                    [
                                        'type_numeric',
                                        ['integer_only_require' => true]
                                    ],
                                    [
                                        'compare_greater',
                                        [
                                            'compare_value' => 0,
                                            'equal_enable_require' => true
                                        ]
                                    ]
                                ]
                            ],
                            'error_message_pattern' => '%1$s must be null or a positive integer.'
                        ]
                    ]
                ],
                ConstAppProfile::ATTRIBUTE_KEY_IMAGE => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-null' => [
                                    [
                                        'compare_equal',
                                        ['compare_value' => ConstNullValue::NULL_VALUE]
                                    ]
                                ],
                                'is-valid-string' => [
                                    'type_string',
                                    [
                                        'sub_rule_not',
                                        [
                                            'rule_config' => ['is_empty']
                                        ]
                                    ]
                                ],
                                'is-valid-file' => [
                                    [
                                        'type_object',
                                        [
                                            'class_path' => [FileInterface::class]
                                        ]
                                    ]
                                ]
                            ],
                            'error_message_pattern' => '%1$s must be null, a string not empty or a valid file.'
                        ]
                    ]
                ],
                ConstAppProfile::ATTRIBUTE_KEY_APP_PROFILE_ATTR_VALUE_ENTITY => [
                    [
                        'group_sub_rule_or',
                        [
                            'rule_config' => [
                                'is-null' => ['is_null'],
                                'is-valid-entity' => [
                                    [
                                        'type_object',
                                        [
                                            'class_path' => [AppProfileAttrValueEntity::class]
                                        ]
                                    ],
                                    ['validation_entity']
                                ]
                            ],
                            'error_message_pattern' => '%1$s must be a valid application profile attribute value entity.'
                        ]
                    ]
                ]
            )
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAppProfile::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAppProfile::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAppProfile::ATTRIBUTE_KEY_ID:
            case ConstAppProfile::ATTRIBUTE_KEY_IMG_SIZE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstAppProfile::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAppProfile::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            ((!is_null($objDtFactory)) && is_string($value)) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstAppProfile::ATTRIBUTE_KEY_NAME:
            case ConstAppProfile::ATTRIBUTE_KEY_SECRET:
            case ConstAppProfile::ATTRIBUTE_KEY_SECRET_VERIFY:
            case ConstAppProfile::ATTRIBUTE_KEY_SECRET_NEW:
            case ConstAppProfile::ATTRIBUTE_KEY_IMG_SRC_NAME:
            case ConstAppProfile::ATTRIBUTE_KEY_IMG_TYPE:
            case ConstAppProfile::ATTRIBUTE_KEY_IMAGE:
            case ConstAppProfile::ATTRIBUTE_KEY_APP_PROFILE_ATTR_VALUE_ENTITY:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstAppProfile::ATTRIBUTE_KEY_SECRET_RESET:
            case ConstAppProfile::ATTRIBUTE_KEY_IS_BLOCKED:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) == 1) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAppProfile::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAppProfile::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrSaveDt($value) :
                        $value
                );
                break;

            case ConstAppProfile::ATTRIBUTE_KEY_IMG_SRC_NAME:
            case ConstAppProfile::ATTRIBUTE_KEY_IMG_TYPE:
            case ConstAppProfile::ATTRIBUTE_KEY_IMG_SIZE:
                $result = ToolBoxNullValue::getAttributeValueSaveFormatGet($value);
                break;

            case ConstAppProfile::ATTRIBUTE_KEY_IMAGE:
                $result = (
                    ($value instanceof FileInterface) ?
                        (
                            ($value instanceof NameFileInterface) ?
                                $value :
                                base64_encode($value->getStrContent())
                        ) :
                        ToolBoxNullValue::getAttributeValueSaveFormatGet($value)
                );
                break;

            case ConstAppProfile::ATTRIBUTE_KEY_APP_PROFILE_ATTR_VALUE_ENTITY:
                $result = (
                    ($value instanceof AppProfileAttrValueEntity) ?
                        $value->getTabDataSave() :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;
        $objAppProfileAttrValueEntityFactory = $this->objAppProfileAttrValueEntityFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAppProfile::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAppProfile::ATTRIBUTE_KEY_DT_UPDATE:
                $result = (
                    ((!is_null($objDtFactory)) && is_string($value)) ?
                        $objDtFactory->getObjDtFromSave($value, true) :
                        $value
                );
                break;

            case ConstAppProfile::ATTRIBUTE_KEY_IMG_SRC_NAME:
            case ConstAppProfile::ATTRIBUTE_KEY_IMG_TYPE:
            case ConstAppProfile::ATTRIBUTE_KEY_IMG_SIZE:
            case ConstAppProfile::ATTRIBUTE_KEY_IMAGE:
                $result = ToolBoxNullValue::getAttributeValueSaveFormatSet($value);
                break;

            case ConstAppProfile::ATTRIBUTE_KEY_APP_PROFILE_ATTR_VALUE_ENTITY:
                $result = $value;
                if((!is_null($objAppProfileAttrValueEntityFactory)) && is_array($value))
                {
                    $objAppProfileAttrValueEntity = $objAppProfileAttrValueEntityFactory->getObjEntity(
                        array(),
                        $this->tabAppProfileAttrValueEntityFactoryExecConfig
                    );

                    // Get data (filter required due to potential different schema)
                    $tabIncludeKey = array_map(
                        function($strKey) use ($objAppProfileAttrValueEntity) {
                            return $objAppProfileAttrValueEntity->getAttributeNameSave($strKey);
                        },
                        $objAppProfileAttrValueEntity->getTabAttributeKey()
                    );
                    $tabData = array_filter(
                        $value,
                        function($key) use ($tabIncludeKey) {
                            return (
                                (!is_string($key)) ||
                                in_array($key, $tabIncludeKey)
                            );
                        },
                        ARRAY_FILTER_USE_KEY
                    );

                    // Hydrate application profile attribute value entity, from data
                    if($objAppProfileAttrValueEntity->hydrateSave($tabData))
                    {
                        $objAppProfileAttrValueEntity->setIsNew(false);
                        $result = $objAppProfileAttrValueEntity;
                    }
                }
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}
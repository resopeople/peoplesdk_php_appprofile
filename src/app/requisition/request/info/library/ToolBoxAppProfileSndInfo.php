<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\app\requisition\request\info\library;

use liberty_code\library\instance\model\Multiton;

use people_sdk\library\requisition\request\info\library\ToolBoxSndInfo;
use people_sdk\app_profile\app\library\ConstAppProfile;
use people_sdk\app_profile\app\model\AppProfileEntity;



class ToolBoxAppProfileSndInfo extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get sending information array,
     * with application basic authentication,
     * from specified application profile entity,
     * to connect to specific application profile, using HTTP basic format.
     *
     * Sending information array format:
     * @see ToolBoxSndInfo::getTabSndInfoWithAuthAppBasic() sending information array format.
     *
     * Return format:
     * @see ToolBoxSndInfo::getTabSndInfoWithAuthAppBasic() return format.
     *
     * @param AppProfileEntity $objAppProfileEntity
     * @param boolean $boolOnHeaderRequired = true
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithAuthAppBasic(
        AppProfileEntity $objAppProfileEntity,
        $boolOnHeaderRequired = true,
        array $tabInfo = null
    )
    {
        // Check arguments
        $objAppProfileEntity->setAttributeValid(ConstAppProfile::ATTRIBUTE_KEY_NAME);
        $objAppProfileEntity->setAttributeValid(ConstAppProfile::ATTRIBUTE_KEY_SECRET);

        // Return result
        return ToolBoxSndInfo::getTabSndInfoWithAuthAppBasic(
            $objAppProfileEntity->getAttributeValue(ConstAppProfile::ATTRIBUTE_KEY_NAME),
            $objAppProfileEntity->getAttributeValue(ConstAppProfile::ATTRIBUTE_KEY_SECRET),
            $boolOnHeaderRequired,
            $tabInfo
        );
    }



}
<?php
/**
 * This class allows to define application profile attribute entity factory class.
 * Application profile attribute entity factory allows to provide new application profile attribute entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\attribute\model;

use people_sdk\library\model\entity\factory\model\DefaultEntityFactory;

use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use liberty_code\handle_model\attribute\specification\api\AttrSpecInterface;
use liberty_code\attribute_model\attribute\library\ConstAttribute;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\app_profile\attribute\model\AppProfileAttrEntity;



/**
 * @method AppProfileAttrEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 */
class AppProfileAttrEntityFactory extends DefaultEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => AppProfileAttrEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => ConstAttribute::ATTRIBUTE_KEY_NAME
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNewEngine(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $objAttrSpec = $this->getObjInstance(AttrSpecInterface::class);
        $result = new AppProfileAttrEntity(
            array(),
            $objValidator,
            $objDateTimeFactory,
            $objAttrSpec
        );

        // Return result
        return $result;
    }



}
<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\app\attribute\value\exception;

use Exception;
use people_sdk\app_profile\attribute\provider\model\AppProfileAttrProvider;
use people_sdk\app_profile\app\attribute\value\library\ConstAppProfileAttrValue;



class AppProfileAttrProviderInvalidFormatException extends Exception
{
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor
	// ******************************************************************************
	
	/**
	 * Constructor
	 * 
	 * @param mixed $provider
     */
	public function __construct($provider)
	{
		// Call parent constructor
		parent::__construct();
		
		// Init var
		$this->message = sprintf
        (
            ConstAppProfileAttrValue::EXCEPT_MSG_APP_PROFILE_ATTR_PROVIDER_INVALID_FORMAT,
            mb_strimwidth(strval($provider), 0, 50, "...")
        );
	}
	
	
	
	
	
	// Methods statics security (throw exception if check not pass)
	// ******************************************************************************
	
	/**
	 * Check if specified provider has valid format.
	 * 
     * @param mixed $provider
	 * @return boolean
	 * @throws static
     */
    public static function setCheck($provider)
    {
        // Init var
        $result =
            // Check valid type
            ($provider instanceof AppProfileAttrProvider);

        // Throw exception if check not pass
        if(!$result)
        {
            throw new static($provider);
        }
		
		// Return result
		return $result;
    }
	
	
	
}
<?php
/**
 * Description :
 * This class allows to define application profile configuration sending information factory class.
 * Application profile configuration sending information factory uses application profile values, from source configuration object,
 * to provide HTTP request sending information.
 *
 * Application profile configuration sending information factory uses the following specified configuration:
 * [
 *     Default configuration sending information factory,
 *
 *     app_profile_role_support_type(optional: got "header", if not found):
 *         "string support type, to set application profile role options parameters.
 *         Scope of available values: @see ConstAppProfileConfigSndInfoFactory::getTabConfigSupportType()",
 *
 *     app_profile_role_perm_full_update_config_key(optional):
 *         "string configuration key, to get application profile role permission full update option
 *         used on application profile role options"
 *
 *     app_profile_role_role_full_update_config_key(optional):
 *         "string configuration key, to get application profile role role full update option
 *         used on application profile role options"
 * ]
 *
 * Application profile configuration sending information factory uses the following values,
 * from specified source configuration object, to get sending information:
 * {
 *     Value <app_profile_role_perm_full_update_config_key>(optional): true / false,
 *
 *     Value <app_profile_role_role_full_update_config_key>(optional): true / false
 * }
 *
 * Note:
 * -> Configuration support type:
 *     - "url_argument":
 *         Data put on URL arguments array.
 *     - "header":
 *         Data put on headers array.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\requisition\request\info\factory\model;

use people_sdk\library\requisition\request\info\factory\config\model\DefaultConfigSndInfoFactory;

use people_sdk\library\requisition\request\info\library\ToolBoxSndInfo;
use people_sdk\library\requisition\request\info\factory\library\ConstSndInfoFactory;
use people_sdk\app_profile\app\requisition\request\info\library\ConstAppProfileSndInfo;
use people_sdk\app_profile\requisition\request\info\factory\library\ConstAppProfileConfigSndInfoFactory;
use people_sdk\app_profile\requisition\request\info\factory\exception\ConfigInvalidFormatException;



class AppProfileConfigSndInfoFactory extends DefaultConfigSndInfoFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        // $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstSndInfoFactory::DATA_KEY_DEFAULT_CONFIG:
                    ConfigInvalidFormatException::setCheck($value);
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
	// ******************************************************************************

    /**
     * Get specified support type.
     *
     * @param string $strConfigKey
     * @param string $strDefault = ConstAppProfileConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
     * @return string
     */
    protected function getStrSupportType(
        $strConfigKey,
        $strDefault = ConstAppProfileConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
    )
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strDefault = (is_string($strDefault) ? $strDefault : ConstAppProfileConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER);
        $result = (
            isset($tabConfig[$strConfigKey]) ?
                $tabConfig[$strConfigKey] :
                $strDefault
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured application profile role permission full update option,
     * used on application profile role options.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithAppProfileRolePermFullUpdate(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstAppProfileConfigSndInfoFactory::TAB_CONFIG_KEY_APP_PROFILE_ROLE_PERM_FULL_UPDATE_CONFIG_KEY]) ?
                $tabConfig[ConstAppProfileConfigSndInfoFactory::TAB_CONFIG_KEY_APP_PROFILE_ROLE_PERM_FULL_UPDATE_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstAppProfileConfigSndInfoFactory::TAB_CONFIG_KEY_APP_PROFILE_ROLE_SUPPORT_TYPE) ==
                    ConstAppProfileConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithParam(
                    ((intval($value) != 0) ? 1 : 0),
                    ($boolOnHeaderRequired ? ConstAppProfileSndInfo::HEADER_KEY_PERM_FULL_UPDATE : null),
                    ((!$boolOnHeaderRequired) ? ConstAppProfileSndInfo::URL_ARG_KEY_PERM_FULL_UPDATE : null)
                );
            },
            $tabInfo,
            function($value){
                return (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($value) ||
                    is_int($value) ||
                    (is_string($value) && ctype_digit($value))
                );
            }
        );

        // Return result
        return $result;
    }



    /**
     * Get new or specified sending information array.
     * with configured application profile role role full update option,
     * used on application profile role options.
     *
     * @param null|array $tabInfo = null
     * @return array
     */
    protected function getTabSndInfoWithAppProfileRoleRoleFullUpdate(array $tabInfo = null)
    {
        // Init var
        $tabConfig = $this->getTabConfig();
        $strKey = (
            isset($tabConfig[ConstAppProfileConfigSndInfoFactory::TAB_CONFIG_KEY_APP_PROFILE_ROLE_ROLE_FULL_UPDATE_CONFIG_KEY]) ?
                $tabConfig[ConstAppProfileConfigSndInfoFactory::TAB_CONFIG_KEY_APP_PROFILE_ROLE_ROLE_FULL_UPDATE_CONFIG_KEY] :
                null
        );
        $result = $this->getTabSndInfoWithSrcConfigValue(
            $strKey,
            function($value){
                $boolOnHeaderRequired = (
                    $this->getStrSupportType(ConstAppProfileConfigSndInfoFactory::TAB_CONFIG_KEY_APP_PROFILE_ROLE_SUPPORT_TYPE) ==
                    ConstAppProfileConfigSndInfoFactory::CONFIG_SUPPORT_TYPE_HEADER
                );

                return ToolBoxSndInfo::getTabSndInfoWithParam(
                    ((intval($value) != 0) ? 1 : 0),
                    ($boolOnHeaderRequired ? ConstAppProfileSndInfo::HEADER_KEY_ROLE_FULL_UPDATE : null),
                    ((!$boolOnHeaderRequired) ? ConstAppProfileSndInfo::URL_ARG_KEY_ROLE_FULL_UPDATE : null)
                );
            },
            $tabInfo,
            function($value){
                return (
                    // Check is boolean or numeric (0 = false, 1 = true)
                    is_bool($value) ||
                    is_int($value) ||
                    (is_string($value) && ctype_digit($value))
                );
            }
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabSndInfoEngine()
    {
        // Init var
        $result = $this->getTabSndInfoWithAppProfileRolePermFullUpdate();
        $result = $this->getTabSndInfoWithAppProfileRoleRoleFullUpdate($result);

        // Return result
        return $result;
    }



}
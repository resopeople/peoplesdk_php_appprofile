<?php
/**
 * This class allows to define application profile attribute entity simple collection repository class.
 * Application profile attribute entity simple collection repository is simple collection repository,
 * allows to prepare loaded data, from requisition persistence, for application profile attribute entity collection.
 * Specified requisition persistence must be able to use HTTP client, persistor HTTP request and persistor HTTP response, with json parsing,
 * to handle HTTP request sending and HTTP response reception.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\attribute\model\repository;

use people_sdk\library\model\repository\simple\model\SimpleCollectionRepository;

use liberty_code\parser\parser\factory\library\ConstParserFactory;
use liberty_code\parser\parser\factory\string_table\library\ConstStrTableParserFactory;
use liberty_code\model\repository\library\ConstRepository as BaseConstRepository;
use liberty_code\requisition\request\factory\library\ConstRequestFactory;
use liberty_code\requisition\response\factory\library\ConstResponseFactory;
use liberty_code\requisition\client\info\library\ConstInfoClient;
use liberty_code\requisition\persistence\library\ConstPersistor;
use liberty_code\http\requisition\request\library\ConstHttpRequest;
use liberty_code\http\requisition\request\data\library\ConstDataHttpRequest;
use liberty_code\http\requisition\request\persistence\library\ConstPersistorHttpRequest;
use liberty_code\http\requisition\request\factory\library\ConstHttpRequestFactory;
use liberty_code\http\requisition\response\data\library\ConstDataHttpResponse;
use liberty_code\http\requisition\response\persistence\library\ConstPersistorHttpResponse;
use liberty_code\http\requisition\response\factory\library\ConstHttpResponseFactory;
use people_sdk\library\requisition\response\library\ConstResponse;
use people_sdk\library\model\entity\requisition\response\library\ConstResponseEntity;
use people_sdk\library\model\repository\library\ConstRepository;
use people_sdk\app_profile\attribute\library\ConstAppProfileAttr;
use people_sdk\app_profile\attribute\model\AppProfileAttrEntityFactory;
use people_sdk\app_profile\attribute\model\repository\AppProfileAttrEntitySimpleRepository;



/**
 * @method null|AppProfileAttrEntityFactory getObjEntityFactory() @inheritdoc
 * @method null|AppProfileAttrEntitySimpleRepository getObjRepository() @inheritdoc
 */
class AppProfileAttrEntitySimpleCollectionRepository extends SimpleCollectionRepository
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|AppProfileAttrEntityFactory $objEntityFactory = null
     * @param null|AppProfileAttrEntitySimpleRepository $objRepository = null
     */
    public function __construct(
        AppProfileAttrEntityFactory $objEntityFactory = null,
        AppProfileAttrEntitySimpleRepository $objRepository = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objEntityFactory,
            $objRepository
        );
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixEntityFactoryClassPath()
    {
        // Return result
        return AppProfileAttrEntityFactory::class;
    }



    /**
     * @inheritdoc
     */
    protected function getStrFixRepositoryClassPath()
    {
        // Return result
        return AppProfileAttrEntitySimpleRepository::class;
    }



    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Init var
        $tabPersistConfig = array(
            ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                ConstRequestFactory::TAB_CONFIG_KEY_TYPE => ConstHttpRequestFactory::CONFIG_TYPE_HTTP_PERSISTOR,
                ConstDataHttpRequest::TAB_CONFIG_KEY_BODY_PARSER_CONFIG => [
                    ConstParserFactory::TAB_CONFIG_KEY_TYPE => ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_JSON
                ],
                ConstPersistorHttpRequest::TAB_CONFIG_KEY_DATA_ID_KEY => ConstAppProfileAttr::ATTRIBUTE_NAME_SAVE_ID
            ],
            ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                    ConstResponseFactory::TAB_CONFIG_KEY_TYPE => ConstHttpResponseFactory::CONFIG_TYPE_HTTP_PERSISTOR,
                    ConstDataHttpResponse::TAB_CONFIG_KEY_BODY_PARSER_CONFIG => [
                        ConstParserFactory::TAB_CONFIG_KEY_TYPE => ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_JSON
                    ],
                    ConstPersistorHttpResponse::TAB_CONFIG_KEY_PATH_SEPARATOR => '/',
                    ConstPersistorHttpResponse::TAB_CONFIG_KEY_DATA_ID_KEY => ConstAppProfileAttr::ATTRIBUTE_NAME_SAVE_ID
                ]
            ]
        );
        $result = array(
            BaseConstRepository::TAB_CONFIG_KEY_TRANSACTION_REQUIRE => false,
            ConstRepository::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_NAME_SAVE_ID => ConstAppProfileAttr::ATTRIBUTE_NAME_SAVE_NAME,

            // Persistence configuration, for persistence action get
            ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_CONFIG => $tabPersistConfig,
            ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_CONFIG => [
                // Additional persistence configuration, for schema sub-action
                ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'GET',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/application-profile/attribute/schema'
                        ]
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_MULTI_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ],
                // Additional persistence configuration, for schema private get sub-action
                ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA_PRIVATE_GET => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'GET',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/application-profile/attribute/schema/private-get'
                        ]
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_MULTI_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ],
                // Additional persistence configuration, for schema private update sub-action
                ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA_PRIVATE_UPDATE => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'GET',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/application-profile/attribute/schema/private-update'
                        ]
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_MULTI_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ],
                // Additional persistence configuration, for schema public get sub-action
                ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA_PUBLIC_GET => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'GET',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/application-profile/attribute/schema/public-get'
                        ]
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_MULTI_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ],
                // Additional persistence configuration, for schema public extended get sub-action
                ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA_PUBLIC_EXTEND_GET => [
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_REQUEST_CONFIG => [
                        ConstRequestFactory::TAB_CONFIG_KEY_SND_INFO => [
                            ConstHttpRequest::TAB_SND_INFO_KEY_METHOD => 'GET',
                            ConstHttpRequest::TAB_SND_INFO_KEY_URL_ROUTE => '/application-profile/attribute/schema/public-extend-get'
                        ]
                    ],
                    ConstPersistor::TAB_EXEC_CONFIG_KEY_CLIENT_EXECUTION_CONFIG => [
                        ConstInfoClient::TAB_CONFIG_KEY_RESPONSE_CONFIG => [
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_SUCCESS_STATUS_CODE => 200,
                            ConstPersistorHttpResponse::TAB_CONFIG_KEY_MULTI_DATA_SELECT_PATH => sprintf(
                                '%1$s/%2$s',
                                ConstResponse::BODY_KEY_RESULT,
                                ConstResponseEntity::COLLECTION_KEY_DATA
                            )
                        ]
                    ]
                ]
            ],
            ConstRepository::TAB_CONFIG_KEY_PERSIST_GET_SUB_ACTION_TYPE => ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA_PUBLIC_GET
        );

        // Return result
        return $result;
    }



}
<?php
/**
 * This class allows to define application profile attribute value entity factory class.
 * Application profile attribute value entity factory allows to provide new application profile attribute value entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\app\attribute\value\model;

use liberty_code\model\entity\factory\fix\model\FixEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory;
use people_sdk\app_profile\attribute\provider\model\AppProfileAttrProvider;
use people_sdk\app_profile\app\attribute\value\library\ConstAppProfileAttrValue;
use people_sdk\app_profile\app\attribute\value\exception\AppProfileAttrProviderInvalidFormatException;
use people_sdk\app_profile\app\attribute\value\model\AppProfileAttrValueEntity;



/**
 * @method AppProfileAttrProvider getObjAppProfileAttrProvider() Get application profile attribute provider object.
 * @method void setObjAppProfileAttrProvider(AppProfileAttrProvider $objAppProfileAttrProvider) Set application profile attribute provider object.
 * @method AppProfileAttrValueEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 */
class AppProfileAttrValueEntityFactory extends FixEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************

	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();





	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param AppProfileAttrProvider $objAppProfileAttrProvider
     */
    public function __construct(
        ProviderInterface $objProvider,
        AppProfileAttrProvider $objAppProfileAttrProvider
    )
    {
        // Call parent constructor
        parent::__construct($objProvider);

        // Init application profile attribute provider
        $this->setObjAppProfileAttrProvider($objAppProfileAttrProvider);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstAppProfileAttrValue::DATA_KEY_DEFAULT_APP_PROFILE_ATTR_PROVIDER))
        {
            $this->__beanTabData[ConstAppProfileAttrValue::DATA_KEY_DEFAULT_APP_PROFILE_ATTR_PROVIDER] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstAppProfileAttrValue::DATA_KEY_DEFAULT_APP_PROFILE_ATTR_PROVIDER,
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstAppProfileAttrValue::DATA_KEY_DEFAULT_APP_PROFILE_ATTR_PROVIDER:
                    AppProfileAttrProviderInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => AppProfileAttrValueEntity::class
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNew(array $tabConfig = null)
    {
        // Init var
        $objAttrProvider = $this->getObjAppProfileAttrProvider();
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $result = new AppProfileAttrValueEntity(
            $objAttrProvider,
            array(),
            $objValidator
        );

        // Return result
        return $result;
    }



}
<?php
/**
 * This class allows to define application profile entity collection class.
 * key => AppProfileEntity.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\app\model;

use liberty_code\model\entity\fix\model\FixEntityCollection;

use people_sdk\app_profile\app\model\AppProfileEntity;



/**
 * @method null|AppProfileEntity getItem(string $strKey) @inheritdoc
 * @method void setItem(AppProfileEntity $objEntity) @inheritdoc
 */
class AppProfileEntityCollection extends FixEntityCollection
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrFixItemClassPath()
    {
        // Return result
        return AppProfileEntity::class;
    }



}
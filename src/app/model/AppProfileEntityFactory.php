<?php
/**
 * This class allows to define application profile entity factory class.
 * Application profile entity factory allows to provide new application profile entities.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\app\model;

use people_sdk\role\profile\model\RoleProfileEntityFactory;

use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\api\EntityCollectionInterface;
use liberty_code\model\entity\factory\library\ConstEntityFactory as BaseConstEntityFactory;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\role\permission\subject\model\SubjPermEntityFactory;
use people_sdk\role\role\model\RoleEntityFactory;
use people_sdk\app_profile\app\attribute\value\model\AppProfileAttrValueEntityFactory;
use people_sdk\app_profile\app\library\ConstAppProfile;
use people_sdk\app_profile\app\exception\AppProfileAttrValueEntityFactoryInvalidFormatException;
use people_sdk\app_profile\app\exception\AppProfileAttrValueEntityFactoryExecConfigInvalidFormatException;
use people_sdk\app_profile\app\model\AppProfileEntity;



/**
 * @method null|AppProfileAttrValueEntityFactory getObjAppProfileAttrValueEntityFactory() Get application profile attribute value entity factory object.
 * @method null|array getTabAppProfileAttrValueEntityFactoryExecConfig() Get application profile attribute value entity factory execution configuration array.
 * @method AppProfileEntity getObjEntity(array $tabValue = array(), array $tabConfig = null) @inheritdoc
 * @method void setObjAppProfileAttrValueEntityFactory(null|AppProfileAttrValueEntityFactory $objAppProfileAttrValueEntityFactory) Set application profile attribute value entity factory object.
 * @method void setTabAppProfileAttrValueEntityFactoryExecConfig(null|array $tabAppProfileAttrValueEntityFactoryExecConfig) Set application profile attribute value entity factory execution configuration array.
 */
class AppProfileEntityFactory extends RoleProfileEntityFactory
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	



	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|AppProfileAttrValueEntityFactory $objAppProfileAttrValueEntityFactory = null
     * @param null|array $tabAppProfileAttrValueEntityFactoryExecConfig = null
     */
    public function __construct(
        ProviderInterface $objProvider,
        EntityCollectionInterface $objEntityCollection = null,
        AppProfileAttrValueEntityFactory $objAppProfileAttrValueEntityFactory = null,
        array $tabAppProfileAttrValueEntityFactoryExecConfig = null,
        array $tabSubjPermEntityFactoryExecConfig = null,
        array $tabRoleEntityFactoryExecConfig = null
    )
    {
        // Call parent constructor
        parent::__construct(
            $objProvider,
            $objEntityCollection,
            $tabSubjPermEntityFactoryExecConfig,
            $tabRoleEntityFactoryExecConfig
        );

        // Init application profile attribute value entity factory
        $this->setObjAppProfileAttrValueEntityFactory($objAppProfileAttrValueEntityFactory);

        // Init application profile attribute value entity factory execution config
        $this->setTabAppProfileAttrValueEntityFactoryExecConfig($tabAppProfileAttrValueEntityFactoryExecConfig);
    }





    // Methods initialize
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function beanHydrateDefault()
    {
        // Init bean data
        if(!$this->beanExists(ConstAppProfile::DATA_KEY_APP_PROFILE_ATTR_VALUE_ENTITY_FACTORY))
        {
            $this->__beanTabData[ConstAppProfile::DATA_KEY_APP_PROFILE_ATTR_VALUE_ENTITY_FACTORY] = null;
        }

        if(!$this->beanExists(ConstAppProfile::DATA_KEY_APP_PROFILE_ATTR_VALUE_ENTITY_FACTORY_EXEC_CONFIG))
        {
            $this->__beanTabData[ConstAppProfile::DATA_KEY_APP_PROFILE_ATTR_VALUE_ENTITY_FACTORY_EXEC_CONFIG] = null;
        }

        // Call parent method
        parent::beanHydrateDefault();
    }





    // Methods validation
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function beanCheckValidKey($key, &$error = null)
    {
        // Init var
        $tabKey = array(
            ConstAppProfile::DATA_KEY_APP_PROFILE_ATTR_VALUE_ENTITY_FACTORY,
            ConstAppProfile::DATA_KEY_APP_PROFILE_ATTR_VALUE_ENTITY_FACTORY_EXEC_CONFIG
        );
        $result =
            in_array($key, $tabKey) ||
            parent::beanCheckValidKey($key, $error);

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    public function beanCheckValidValue($key, $value, &$error = null)
    {
        // Init var
        $result = true;

        // Validation
        try
        {
            switch($key)
            {
                case ConstAppProfile::DATA_KEY_APP_PROFILE_ATTR_VALUE_ENTITY_FACTORY:
                    AppProfileAttrValueEntityFactoryInvalidFormatException::setCheck($value);
                    break;

                case ConstAppProfile::DATA_KEY_APP_PROFILE_ATTR_VALUE_ENTITY_FACTORY_EXEC_CONFIG:
                    AppProfileAttrValueEntityFactoryExecConfigInvalidFormatException::setCheck($value);
                    break;

                default:
                    $result = parent::beanCheckValidValue($key, $value, $error);
                    break;
            }
        }
        catch(\Exception $e)
        {
            $result = false;
            $error = $e;
        }

        // Return result
        return $result;
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            BaseConstEntityFactory::TAB_CONFIG_KEY_ENTITY_CLASS_PATH => AppProfileEntity::class,
            ConstEntityFactory::TAB_CONFIG_KEY_SELECT_ENTITY_ATTRIBUTE_KEY_ID => ConstAppProfile::ATTRIBUTE_KEY_ID
        );
    }



    /**
     * @inheritdoc
     */
    protected function getObjEntityNewEngine(array $tabConfig = null)
    {
        // Init var
        $objValidator = $this->getObjInstance(ValidatorInterface::class);
        $objDateTimeFactory = $this->getObjInstance(DateTimeFactoryInterface::class);
        $objSubjPermEntityFactory = $this->getObjInstance(SubjPermEntityFactory::class);
        $objRoleEntityFactory = $this->getObjInstance(RoleEntityFactory::class);
        $result = new AppProfileEntity(
            array(),
            $objValidator,
            $objDateTimeFactory,
            $this->getObjAppProfileAttrValueEntityFactory(),
            $objSubjPermEntityFactory,
            $objRoleEntityFactory,
            $this->getTabAppProfileAttrValueEntityFactoryExecConfig(),
            $this->getTabSubjPermEntityFactoryExecConfig(),
            $this->getTabRoleEntityFactoryExecConfig()
        );

        // Return result
        return $result;
    }



}
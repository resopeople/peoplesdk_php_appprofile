<?php
/**
 * This class allows to define application profile token key entity class.
 * Application profile token key entity allows to design a specific temporary authentication key entity,
 * used for application profile entity authentication.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\token\model;

use liberty_code\model\entity\repository\model\SaveConfigEntity;

use DateTime;
use liberty_code\validation\validator\api\ValidatorInterface;
use liberty_code\model\entity\library\ConstEntity;
use liberty_code\model\entity\repository\library\ConstSaveEntity;
use liberty_code\model\datetime\factory\api\DateTimeFactoryInterface;
use people_sdk\library\model\entity\library\ToolBoxEntity;
use people_sdk\library\model\entity\factory\library\ConstEntityFactory;
use people_sdk\app_profile\app\library\ConstAppProfile;
use people_sdk\app_profile\app\model\AppProfileEntity;
use people_sdk\app_profile\app\model\AppProfileEntityFactory;
use people_sdk\app_profile\token\library\ConstAppProfileTokenKey;



/**
 * @property null|integer $intAttrId
 * @property null|string|DateTime $attrDtCreate
 * @property null|string|DateTime $attrDtUpdate
 * @property null|string|DateTime $attrDtValidStart
 * @property null|string|DateTime $attrDtValidEnd
 * @property null|string $strAttrName
 * @property null|string $strAttrKey
 * @property null|boolean $boolAttrKeyReset
 * @property null|integer|AppProfileEntity $attrAppProfile : application profile id|entity
 */
class AppProfileTokenKeyEntity extends SaveConfigEntity
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Datetime factory instance.
     * @var null|DateTimeFactoryInterface
     */
    protected $objDateTimeFactory;



    /**
     * DI: Application profile entity factory instance.
     * @var null|AppProfileEntityFactory
     */
    protected $objAppProfileEntityFactory;



    /**
     * DI: Application profile entity factory execution configuration.
     * @var null|array
     */
    protected $tabAppProfileEntityFactoryExecConfig;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param null|DateTimeFactoryInterface $objDateTimeFactory = null
     * @param null|AppProfileEntityFactory $objAppProfileEntityFactory = null
     * @param null|array $tabAppProfileEntityFactoryExecConfig = null
     */
    public function __construct(
        array $tabValue = array(),
        ValidatorInterface $objValidator = null,
        DateTimeFactoryInterface $objDateTimeFactory = null,
        AppProfileEntityFactory $objAppProfileEntityFactory = null,
        array $tabAppProfileEntityFactoryExecConfig = null
    )
    {
        // Init properties
        $this->objDateTimeFactory = $objDateTimeFactory;
        $this->objAppProfileEntityFactory = $objAppProfileEntityFactory;
        $this->tabAppProfileEntityFactoryExecConfig = $tabAppProfileEntityFactoryExecConfig;

        // Call parent constructor
        parent::__construct($tabValue, $objValidator);
    }





    // Methods entity overwrite
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getStrBaseKey()
    {
        // Init var
        $intId = $this->getAttributeValue(ConstAppProfileTokenKey::ATTRIBUTE_KEY_ID);
        $result = (
        (is_int($intId) && ($intId > 0)) ?
            strval($intId) :
            parent::getStrBaseKey()
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getTabConfig()
    {
        // Return result
        return array(
            // Attribute id
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAppProfileTokenKey::ATTRIBUTE_KEY_ID,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAppProfileTokenKey::ATTRIBUTE_ALIAS_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAppProfileTokenKey::ATTRIBUTE_NAME_SAVE_ID,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime create
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_CREATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAppProfileTokenKey::ATTRIBUTE_ALIAS_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAppProfileTokenKey::ATTRIBUTE_NAME_SAVE_DT_CREATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime update
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_UPDATE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAppProfileTokenKey::ATTRIBUTE_ALIAS_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAppProfileTokenKey::ATTRIBUTE_NAME_SAVE_DT_UPDATE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET => false,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime validity start
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_VALID_START,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAppProfileTokenKey::ATTRIBUTE_ALIAS_DT_VALID_START,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAppProfileTokenKey::ATTRIBUTE_NAME_SAVE_DT_VALID_START,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute datetime validity end
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_VALID_END,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAppProfileTokenKey::ATTRIBUTE_ALIAS_DT_VALID_END,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAppProfileTokenKey::ATTRIBUTE_NAME_SAVE_DT_VALID_END,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute name
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAppProfileTokenKey::ATTRIBUTE_KEY_NAME,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAppProfileTokenKey::ATTRIBUTE_ALIAS_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAppProfileTokenKey::ATTRIBUTE_NAME_SAVE_NAME,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute key
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAppProfileTokenKey::ATTRIBUTE_KEY_KEY,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAppProfileTokenKey::ATTRIBUTE_ALIAS_KEY,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAppProfileTokenKey::ATTRIBUTE_NAME_SAVE_KEY,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute key reset
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAppProfileTokenKey::ATTRIBUTE_KEY_KEY_RESET,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAppProfileTokenKey::ATTRIBUTE_ALIAS_KEY_RESET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAppProfileTokenKey::ATTRIBUTE_NAME_SAVE_KEY_RESET,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => false,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ],

            // Attribute application profile (id|entity)
            [
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_KEY =>
                    ConstAppProfileTokenKey::ATTRIBUTE_KEY_APP_PROFILE,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_ALIAS =>
                    ConstAppProfileTokenKey::ATTRIBUTE_ALIAS_APP_PROFILE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_NAME_SAVE =>
                    ConstAppProfileTokenKey::ATTRIBUTE_NAME_SAVE_APP_PROFILE,
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_GET =>
                    [ToolBoxEntity::class, 'checkEntityAttrSaveGetEnabled'],
                ConstSaveEntity::TAB_CONFIG_KEY_ATTRIBUTE_SAVABLE_SET => true,
                ConstEntity::TAB_CONFIG_KEY_ATTRIBUTE_DEFAULT_VALUE => null
            ]
        );
    }



    /**
     * @inheritdoc
     */
    protected function getTabRuleConfig()
    {
        // Init rule configuration
        $tabRuleConfigValidString = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-string' => [
                            'type_string',
                            [
                                'sub_rule_not',
                                [
                                    'rule_config' => ['is_empty']
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a string not empty'
                ]
            ]
        );
        $tabRuleConfigValidDate = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-date' => ['type_date']
                    ],
                    'error_message_pattern' => '%1$s must be null or a date.'
                ]
            ]
        );
        $tabRuleConfigValidBoolean = array(
            [
                'group_sub_rule_or',
                [
                    'rule_config' => [
                        'is-null' => ['is_null'],
                        'is-valid-boolean' => [
                            [
                                'type_boolean',
                                [
                                    'integer_enable_require' => false,
                                    'string_enable_require' => false
                                ]
                            ]
                        ]
                    ],
                    'error_message_pattern' => '%1$s must be null or a boolean.'
                ]
            ]
        );

        // Init var
        $result = array(
            ConstAppProfileTokenKey::ATTRIBUTE_KEY_ID => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => [
                                'is_null',
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return $this->checkIsNew();}
                                    ]
                                ]
                            ],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ],
                                [
                                    'callable',
                                    [
                                        'valid_callable' => function() {return (!$this->checkIsNew());}
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be null if new, or a valid ID else.'
                    ]
                ]
            ],
            ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_CREATE => $tabRuleConfigValidDate,
            ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_UPDATE => $tabRuleConfigValidDate,
            ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_VALID_START => $tabRuleConfigValidDate,
            ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_VALID_END => $tabRuleConfigValidDate,
            ConstAppProfileTokenKey::ATTRIBUTE_KEY_NAME => $tabRuleConfigValidString,
            ConstAppProfileTokenKey::ATTRIBUTE_KEY_KEY => $tabRuleConfigValidString,
            ConstAppProfileTokenKey::ATTRIBUTE_KEY_KEY_RESET => $tabRuleConfigValidBoolean,
            ConstAppProfileTokenKey::ATTRIBUTE_KEY_APP_PROFILE => [
                [
                    'group_sub_rule_or',
                    [
                        'rule_config' => [
                            'is-null' => ['is_null'],
                            'is-valid-id' => [
                                [
                                    'type_numeric',
                                    ['integer_only_require' => true]
                                ],
                                [
                                    'compare_greater',
                                    [
                                        'compare_value' => 0,
                                        'equal_enable_require' => false
                                    ]
                                ]
                            ],
                            'is-valid-entity' => [
                                [
                                    'type_object',
                                    [
                                        'class_path' => [AppProfileEntity::class]
                                    ]
                                ],
                                [
                                    'validation_entity',
                                    [
                                        'attribute_key' => ConstAppProfile::ATTRIBUTE_KEY_ID
                                    ]
                                ],
                                [
                                    'new_entity',
                                    [
                                        'new_require' => false
                                    ]
                                ]
                            ]
                        ],
                        'error_message_pattern' => '%1$s must be a valid application profile ID or entity.'
                    ]
                ]
            ]
        );

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_UPDATE:
            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_VALID_START:
            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_VALID_END:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrGetDt($value) :
                        $value
                );
                break;

            default:
                $result = parent::getAttributeValueFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_ID:
            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_APP_PROFILE:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (is_string($value) && ctype_digit($value)) ?
                                intval($value) :
                                $value
                        )
                );
                break;

            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_UPDATE:
            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_VALID_START:
            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_VALID_END:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            ((!is_null($objDtFactory)) && is_string($value)) ?
                                $objDtFactory->getObjDtFromSet($value, true) :
                                (
                                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                                        $objDtFactory->getObjRefDt($value) :
                                        $value
                                )
                        )
                );
                break;

            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_NAME:
            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_KEY:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        $value
                );
                break;

            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_KEY_RESET:
                $result = (
                    (is_string($value) && (trim($value) === '')) ?
                        null :
                        (
                            (
                                (
                                    is_int($value) ||
                                    (is_string($value) && ctype_digit($value))
                                ) &&
                                in_array(intval($value), array(0, 1))
                            ) ?
                                (intval($value) === 1) :
                                $value
                        )
                );
                break;

            default:
                $result = parent::getAttributeValueFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatGet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_UPDATE:
            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_VALID_START:
            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_VALID_END:
                $result = (
                    ((!is_null($objDtFactory)) && ($value instanceof DateTime)) ?
                        $objDtFactory->getStrSaveDt($value) :
                        $value
                );
                break;

            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_APP_PROFILE:
                $result = array(
                    ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID => (
                        ($value instanceof AppProfileEntity) ?
                            $value->getAttributeValueSave(ConstAppProfile::ATTRIBUTE_KEY_ID) :
                            $value
                    )
                );
                break;

            default:
                $result = parent::getAttributeValueSaveFormatGet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



    /**
     * @inheritdoc
     */
    protected function getAttributeValueSaveFormatSet($strKey, $value)
    {
        // Init var
        $objDtFactory = $this->objDateTimeFactory;
        $objAppProfileEntityFactory = $this->objAppProfileEntityFactory;

        // Format by attribute
        switch($strKey)
        {
            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_CREATE:
            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_UPDATE:
            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_VALID_START:
            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_DT_VALID_END:
                $result = (
                    ((!is_null($objDtFactory)) && is_string($value)) ?
                        $objDtFactory->getObjDtFromSave($value, true) :
                        $value
                );
                break;

            case ConstAppProfileTokenKey::ATTRIBUTE_KEY_APP_PROFILE:
                $result = $value;
                if((!is_null($objAppProfileEntityFactory)) && is_array($value))
                {
                    $objAppProfileEntity = $objAppProfileEntityFactory->getObjEntity(
                        array(),
                        // Try to select application profile entity, by id, if required
                        (
                            (!is_null($this->tabAppProfileEntityFactoryExecConfig)) ?
                                (
                                    isset($value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array_merge(
                                            array(
                                                ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                    $value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID]
                                            ),
                                            $this->tabAppProfileEntityFactoryExecConfig
                                        ) :
                                        $this->tabAppProfileEntityFactoryExecConfig
                                ) :
                                (
                                    isset($value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID]) ?
                                        array(
                                            ConstEntityFactory:: TAB_EXEC_CONFIG_KEY_SELECT_ENTITY_ID =>
                                                $value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID]
                                        ) :
                                        null
                                )
                        )
                    );
                    if($objAppProfileEntity->hydrateSave($value))
                    {
                        $objAppProfileEntity->setIsNew(false);
                        $result = $objAppProfileEntity;
                    }
                }
                else if(isset($value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID]))
                {
                    $result = $value[ConstAppProfile::ATTRIBUTE_NAME_SAVE_ID];
                }
                break;

            default:
                $result = parent::getAttributeValueSaveFormatSet($strKey, $value);
                break;
        }

        // Return result
        return $result;
    }



}
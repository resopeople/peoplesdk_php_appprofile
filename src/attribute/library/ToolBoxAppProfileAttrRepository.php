<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\attribute\library;

use liberty_code\library\instance\model\Multiton;

use liberty_code\model\entity\library\ConstEntity;
use liberty_code\attribute_model\attribute\library\ConstAttribute;
use people_sdk\library\table\library\ToolBoxTable;
use people_sdk\library\model\repository\library\ConstRepository;
use people_sdk\app_profile\attribute\library\ConstAppProfileAttr;
use people_sdk\app_profile\attribute\model\AppProfileAttrEntity;
use people_sdk\app_profile\attribute\model\AppProfileAttrEntityCollection;
use people_sdk\app_profile\attribute\model\repository\AppProfileAttrEntitySimpleCollectionRepository;



class ToolBoxAppProfileAttrRepository extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get execution configuration array,
     * with specified schema engine.
     *
     * Configuration array format:
     * Null or @see AppProfileAttrEntitySimpleCollectionRepository::load() configuration array format.
     *
     * Return format:
     * @see AppProfileAttrEntitySimpleCollectionRepository::load() configuration array format.
     *
     * @param string $strSubActionType
     * @param null|array $tabConfig = null
     * @return null|array
     */
    public static function getTabExecConfigWithSchemaEngine($strSubActionType, array $tabConfig = null)
    {
        // Init var
        $tabSrcConfig = $tabConfig;
        $tabConfig = (
            (
                is_string($strSubActionType) &&
                in_array(
                    $strSubActionType,
                    array(
                        ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA,
                        ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA_PRIVATE_GET,
                        ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA_PRIVATE_UPDATE,
                        ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA_PUBLIC_GET,
                        ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA_PUBLIC_EXTEND_GET
                    )
                )
            ) ?
                array(ConstRepository::TAB_EXEC_CONFIG_KEY_PERSIST_SUB_ACTION_TYPE => $strSubActionType) :
                null
        );
        $result = ToolBoxTable::getTabItemFromSrc($tabConfig, $tabSrcConfig);

        // Return result
        return $result;
    }



    /**
     * Get execution configuration array,
     * with schema.
     *
     * Configuration array format:
     * @see getTabExecConfigWithSchemaEngine() configuration array format.
     *
     * Return format:
     * @see getTabExecConfigWithSchemaEngine() return format.
     *
     * @param null|array $tabConfig = null
     * @return array
     */
    public static function getTabExecConfigWithSchema(array $tabConfig = null)
    {
        // Return result
        return static::getTabExecConfigWithSchemaEngine(ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA, $tabConfig);
    }



    /**
     * Get execution configuration array,
     * with schema private get.
     *
     * Configuration array format:
     * @see getTabExecConfigWithSchemaEngine() configuration array format.
     *
     * Return format:
     * @see getTabExecConfigWithSchemaEngine() return format.
     *
     * @param null|array $tabConfig = null
     * @return array
     */
    public static function getTabExecConfigWithSchemaPrivateGet(array $tabConfig = null)
    {
        // Return result
        return static::getTabExecConfigWithSchemaEngine(ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA_PRIVATE_GET, $tabConfig);
    }



    /**
     * Get execution configuration array,
     * with schema private update.
     *
     * Configuration array format:
     * @see getTabExecConfigWithSchemaEngine() configuration array format.
     *
     * Return format:
     * @see getTabExecConfigWithSchemaEngine() return format.
     *
     * @param null|array $tabConfig = null
     * @return array
     */
    public static function getTabExecConfigWithSchemaPrivateUpdate(array $tabConfig = null)
    {
        // Return result
        return static::getTabExecConfigWithSchemaEngine(ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA_PRIVATE_UPDATE, $tabConfig);
    }



    /**
     * Get execution configuration array,
     * with schema public get.
     *
     * Configuration array format:
     * @see getTabExecConfigWithSchemaEngine() configuration array format.
     *
     * Return format:
     * @see getTabExecConfigWithSchemaEngine() return format..
     *
     * @param null|array $tabConfig = null
     * @return array
     */
    public static function getTabExecConfigWithSchemaPublicGet(array $tabConfig = null)
    {
        // Return result
        return static::getTabExecConfigWithSchemaEngine(ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA_PUBLIC_GET, $tabConfig);
    }



    /**
     * Get execution configuration array,
     * with schema public extended get.
     *
     * Configuration array format:
     * @see getTabExecConfigWithSchemaEngine() configuration array format.
     *
     * Return format:
     * @see getTabExecConfigWithSchemaEngine() return format.
     *
     * @param null|array $tabConfig = null
     * @return array
     */
    public static function getTabExecConfigWithSchemaPublicExtendGet(array $tabConfig = null)
    {
        // Return result
        return static::getTabExecConfigWithSchemaEngine(ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA_PUBLIC_EXTEND_GET, $tabConfig);
    }





    // Methods repository
    // ******************************************************************************

    /**
     * Load specified application profile attribute entity collection,
     * from specified application profile attribute entity simple collection repository,
     * to get specified schema engine.
     * Return true if success, false if an error occurs.
     *
     * Configuration array format:
     * @see getTabExecConfigWithSchemaEngine() configuration array format.
     *
     * @param AppProfileAttrEntityCollection $objAppProfileAttrEntityCollection
     * @param AppProfileAttrEntitySimpleCollectionRepository $objAppProfileAttrEntityCollectionRepository
     * @param string $strSubActionType
     * @param null|array $tabConfig = null
     * @return boolean
     */
    public static function loadSchemaEngine(
        AppProfileAttrEntityCollection $objAppProfileAttrEntityCollection,
        AppProfileAttrEntitySimpleCollectionRepository $objAppProfileAttrEntityCollectionRepository,
        $strSubActionType,
        array $tabConfig = null
    )
    {
        // Init var
        $tabConfig = static::getTabExecConfigWithSchemaEngine($strSubActionType, $tabConfig);
        $tabInitAppProfileAttrEntity = array_values($objAppProfileAttrEntityCollection->getTabItem());

        // Load schema
        $objAppProfileAttrEntityCollection->removeItemAll();
        $result = (
            (!is_null($tabConfig)) ?
                $objAppProfileAttrEntityCollectionRepository->load(
                    $objAppProfileAttrEntityCollection,
                    array(),
                    $tabConfig
                ) :
                false
        );

        // Set initial application profile attributes
        $tabInitAppProfileAttrEntity = (
            $result ?
                array_filter(
                    $tabInitAppProfileAttrEntity,
                    function(AppProfileAttrEntity $objAppProfileAttrEntity) use ($objAppProfileAttrEntityCollection) {
                        $tabConfig = array([
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_ATTRIBUTE_KEY => ConstAttribute::ATTRIBUTE_KEY_NAME,
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_OPERATION => ConstEntity::COLLECTION_GET_CONFIG_OPERATION_EQUAL,
                            ConstEntity::TAB_COLLECTION_GET_CONFIG_KEY_VALUE => $objAppProfileAttrEntity->getAttributeValue(ConstAttribute::ATTRIBUTE_KEY_NAME)
                        ]);

                        return (count($objAppProfileAttrEntityCollection->getTabItem($tabConfig)) == 0);
                    }
                ) :
                $tabInitAppProfileAttrEntity
        );
        $objAppProfileAttrEntityCollection->setTabItem($tabInitAppProfileAttrEntity);

        // Reorder application profile attributes, if required
        if($result)
        {
            $tabAppProfileAttrEntity = $objAppProfileAttrEntityCollection->getTabSortItem();

            $objAppProfileAttrEntityCollection->removeItemAll();
            $objAppProfileAttrEntityCollection->setTabItem($tabAppProfileAttrEntity);
        }

        // Return result
        return $result;
    }



    /**
     * Load specified application profile attribute entity collection,
     * from specified application profile attribute entity simple collection repository,
     * to get specified schemas.
     * Return true if success, false if an error occurs.
     *
     * Configuration array format:
     * @see loadSchemaEngine() configuration array format.
     *
     * @param AppProfileAttrEntityCollection $objAppProfileAttrEntityCollection
     * @param AppProfileAttrEntitySimpleCollectionRepository $objAppProfileAttrEntityCollectionRepository
     * @param boolean $boolSchemaRequired = false
     * @param boolean $boolSchemaPrivateGetRequired = false
     * @param boolean $boolSchemaPrivateUpdateRequired = false
     * @param boolean $boolSchemaPublicGetRequired = false
     * @param boolean $boolSchemaPublicExtendGetRequired = false
     * @param null|array $tabConfig = null
     * @return boolean
     */
    public static function loadSchema(
        AppProfileAttrEntityCollection $objAppProfileAttrEntityCollection,
        AppProfileAttrEntitySimpleCollectionRepository $objAppProfileAttrEntityCollectionRepository,
        $boolSchemaRequired = false,
        $boolSchemaPrivateGetRequired = false,
        $boolSchemaPrivateUpdateRequired = false,
        $boolSchemaPublicGetRequired = false,
        $boolSchemaPublicExtendGetRequired = false,
        array $tabConfig = null
    )
    {
        // Init var
        $boolSchemaRequired =(is_bool($boolSchemaRequired) ? $boolSchemaRequired : false);
        $boolSchemaPrivateGetRequired =(is_bool($boolSchemaPrivateGetRequired) ? $boolSchemaPrivateGetRequired : false);
        $boolSchemaPrivateUpdateRequired =(is_bool($boolSchemaPrivateUpdateRequired) ? $boolSchemaPrivateUpdateRequired : false);
        $boolSchemaPublicGetRequired =(is_bool($boolSchemaPublicGetRequired) ? $boolSchemaPublicGetRequired : false);
        $boolSchemaPublicExtendGetRequired =(is_bool($boolSchemaPublicExtendGetRequired) ? $boolSchemaPublicExtendGetRequired : false);
        $result = true;

        // Load schema, if required
        if($boolSchemaRequired)
        {
            $result = static::loadSchemaEngine(
                $objAppProfileAttrEntityCollection,
                $objAppProfileAttrEntityCollectionRepository,
                ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA,
                $tabConfig
            ) && $result;
        }

        // Load schema private get, if required
        if($boolSchemaPrivateGetRequired)
        {
            $result = static::loadSchemaEngine(
                $objAppProfileAttrEntityCollection,
                $objAppProfileAttrEntityCollectionRepository,
                ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA_PRIVATE_GET,
                $tabConfig
            ) && $result;
        }

        // Load schema private update, if required
        if($boolSchemaPrivateUpdateRequired)
        {
            $result = static::loadSchemaEngine(
                $objAppProfileAttrEntityCollection,
                $objAppProfileAttrEntityCollectionRepository,
                ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA_PRIVATE_UPDATE,
                $tabConfig
            ) && $result;
        }

        // Load schema public get, if required
        if($boolSchemaPublicGetRequired)
        {
            $result = static::loadSchemaEngine(
                $objAppProfileAttrEntityCollection,
                $objAppProfileAttrEntityCollectionRepository,
                ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA_PUBLIC_GET,
                $tabConfig
            ) && $result;
        }

        // Load schema public extended get, if required
        if($boolSchemaPublicExtendGetRequired)
        {
            $result = static::loadSchemaEngine(
                $objAppProfileAttrEntityCollection,
                $objAppProfileAttrEntityCollectionRepository,
                ConstAppProfileAttr::SUB_ACTION_TYPE_SCHEMA_PUBLIC_EXTEND_GET,
                $tabConfig
            ) && $result;
        }

        // Return result
        return $result;
    }



}
<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\api\requisition\request\info\library;

use liberty_code\library\instance\model\Multiton;

use people_sdk\library\requisition\request\info\library\ToolBoxSndInfo;
use people_sdk\app_profile\api\library\ConstAppProfileApiKey;
use people_sdk\app_profile\api\model\AppProfileApiKeyEntity;



class ToolBoxAppProfileApiKeySndInfo extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get sending information array,
     * with application API key authentication,
     * from specified application profile API key entity,
     * to connect to specific application profile, using HTTP API key format.
     *
     * Sending information array format:
     * @see ToolBoxSndInfo::getTabSndInfoWithAuthAppApiKey() sending information array format.
     *
     * Return format:
     * @see ToolBoxSndInfo::getTabSndInfoWithAuthAppApiKey() return format.
     *
     * @param AppProfileApiKeyEntity $objAppProfileApiKeyEntity
     * @param boolean $boolOnHeaderRequired = true
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithAuthAppApiKey(
        AppProfileApiKeyEntity $objAppProfileApiKeyEntity,
        $boolOnHeaderRequired = true,
        array $tabInfo = null
    )
    {
        // Check argument
        $objAppProfileApiKeyEntity->setAttributeValid(ConstAppProfileApiKey::ATTRIBUTE_KEY_KEY);

        // Return result
        return ToolBoxSndInfo::getTabSndInfoWithAuthAppApiKey(
            $objAppProfileApiKeyEntity->getAttributeValue(ConstAppProfileApiKey::ATTRIBUTE_KEY_KEY),
            $boolOnHeaderRequired,
            $tabInfo
        );
    }



}
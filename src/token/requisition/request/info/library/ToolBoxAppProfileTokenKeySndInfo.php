<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\app_profile\token\requisition\request\info\library;

use liberty_code\library\instance\model\Multiton;

use people_sdk\library\requisition\request\info\library\ToolBoxSndInfo;
use people_sdk\app_profile\token\library\ConstAppProfileTokenKey;
use people_sdk\app_profile\token\model\AppProfileTokenKeyEntity;



class ToolBoxAppProfileTokenKeySndInfo extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get sending information array,
     * with application bearer authentication,
     * from specified application profile token key entity,
     * to connect to specific application profile, using HTTP bearer format.
     *
     * Sending information array format:
     * @see ToolBoxSndInfo::getTabSndInfoWithAuthAppBearer() sending information array format.
     *
     * Return format:
     * @see ToolBoxSndInfo::getTabSndInfoWithAuthAppBearer() return format.
     *
     * @param AppProfileTokenKeyEntity $objAppProfileTokenKeyEntity
     * @param boolean $boolOnHeaderRequired = true
     * @param null|array $tabInfo = null
     * @return null|array
     */
    public static function getTabSndInfoWithAuthAppBearer(
        AppProfileTokenKeyEntity $objAppProfileTokenKeyEntity,
        $boolOnHeaderRequired = true,
        array $tabInfo = null
    )
    {
        // Check argument
        $objAppProfileTokenKeyEntity->setAttributeValid(ConstAppProfileTokenKey::ATTRIBUTE_KEY_KEY);

        // Return result
        return ToolBoxSndInfo::getTabSndInfoWithAuthAppBearer(
            $objAppProfileTokenKeyEntity->getAttributeValue(ConstAppProfileTokenKey::ATTRIBUTE_KEY_KEY),
            $boolOnHeaderRequired,
            $tabInfo
        );
    }



}